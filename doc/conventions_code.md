Conventions de codes
=====================

**Code**

 - Tous les noms de variable sont en anglais.
 - Utiliser camelCase, pas de soulignement,  pour les noms de variables, fonction et de m�thode, les arguments;
 - Dans une classe, commencer par les m�thodes publiques, les protected puis les private.
 - Suffixer les interfaces avec Interface
 - Suffixer les exceptions avec Exception
 - Suffixer les classes abstraites avec Abstract
 - Ajouter un espace avant et apr�s les op�rateurs.
 - Vous pouvez utiliser des [conditions Yoda](https://en.wikipedia.org/wiki/Yoda_conditions) pour �viter les erreurs d'affectation.
 
 
**Git**
 
 - Les commit sont push�s dans sa branche personnelle, pas dans le master.
 - Les commits doivent respecter les r�gles �l�mentaires de git : (atomiques, comment�s).
 - Le code commit� doit toujours compiler (pas de commit de code ne compilant pas) pour garantir la coh�rence du projet.