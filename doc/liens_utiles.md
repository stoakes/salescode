Liens Utiles
======

Liens utiles class�s par centre d'interet.

[Slack interne salescodeteam.slack.com](https://salescodeteam.slack.com)

Front-end
---------
[Intro rapide � Angular JS](http://www.tutoriel-angularjs.fr/)



Back-end
---------

- [Deployer une appli Spring sur jboss](https://blog.openshift.com/spring-mvc-3-on-jboss)


Exemples et inspiration
-----------------------


**Page d'accueil**

http://whois.domaintools.com

[Code exemple d'une appli en JEE + Spring MVC et Angular](https://github.com/jhades/spring-mvc-angularjs-sample-app)