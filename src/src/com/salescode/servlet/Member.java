package com.salescode.servlet;

import java.io.IOException;
import java.util.List;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.salescode.bean.UserBean;
import com.salescode.entity.User;

/**
 * Servlet implementation class ShoppingCart
 */
@WebServlet("/member")
public class Member extends HttpServlet {
	private static final long serialVersionUID = 1L;
	public static final String CONNEXION = "/WEB-INF/views/connexion.jsp";
	public static final String VUE = "/WEB-INF/views/member.jsp";

    
	
	@EJB
	UserBean userBean;
	/**
     * @see HttpServlet#HttpServlet()
     */
    public Member() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		//Test si le user est connect� sinon go page de connexion
		
		if(session.getAttribute("user") == null){ 
			
    		this.getServletContext().getRequestDispatcher(Member.CONNEXION ).forward( request, response );
    	} 
    		List<User> listUser=userBean.retrieveAllUser();
    		request.setAttribute("listUser",listUser);
    		User u= (User)session.getAttribute("user");
    		request.setAttribute("currentUser",u);
    		this.getServletContext().getRequestDispatcher( Member.VUE ).forward( request, response );
    	
		
	
		
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
