package com.salescode.servlet;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.salescode.bean.UserBean;
import com.salescode.entity.User;
import com.salescode.form.ProfilForm;

/**
 * Servlet implementation class Profil
 */

@WebServlet("/profile")
public class Profil extends HttpServlet {

	public static final String VUE_AFFICHAGE = "/WEB-INF/views/profile/affichageProfil.jsp";
	public static final String VUE_EDITION = "/WEB-INF/views/profile/editionProfil.jsp";

	@EJB
	UserBean userbean;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		User u = (User)request.getSession().getAttribute("user");
		boolean editable;

		if(u != null){ //s'il est connecté
			String stid = request.getParameter("id");
			String op = request.getParameter("op");

			if(op != null){
				//on edite : 
				/* Affichage de la page d'edition de profil */
				request.setAttribute("user",u);
				request.setAttribute("editable", true);
				this.getServletContext().getRequestDispatcher( VUE_EDITION ).forward( request, response );
			}
			else{ // on ne souhaite pas editer

				if(stid != null){//on a l'id du user a afficher
					int id = Integer.parseInt(stid);
					User user = userbean.findById(id);
					if(user == null){
						this.getServletContext().getRequestDispatcher("/WEB-INF/views/404.jsp").forward(request, response);
					}
					else{
						request.setAttribute("user",  user);
						request.setAttribute("editable", false);
						this.getServletContext().getRequestDispatcher( VUE_AFFICHAGE ).forward( request, response );
					}
				} 
				else { //on a pas d'id d'utilisateur : on affiche son profil
					editable = true;
					request.setAttribute("user",userbean.find(u));
					request.setAttribute("editable", true);
					this.getServletContext().getRequestDispatcher( VUE_AFFICHAGE).forward( request, response );
				}
			}
		} else {
			response.sendRedirect("/salescode/connexion");
		}

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		HttpSession session = request.getSession();
		User u = (User)request.getSession().getAttribute("user");
		String message = "";
		if(u != null){ // est ce que l'utilisateur est connecté ?
			/* creation du form pour le traitement du form */
			ProfilForm form = new ProfilForm(userbean);
			try {
				u = form.registerNewEditUser(request, u);
				System.out.println(u);
				userbean.merge(u);
				message = "Profil modifié";
				request.setAttribute( "user", u );
			} catch (Exception e) {
				e.printStackTrace();
			}
	
			/* Stockage du formulaire et du bean dans l'objet request */
			request.setAttribute( "form", form );
			this.getServletContext().getRequestDispatcher( VUE_EDITION ).forward( request, response );
		
		}else
		{
			response.sendRedirect("/salescode/connexion");
		}
	}

}