package com.salescode.servlet;

import java.io.File;
import java.io.IOException;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.eclipse.jgit.api.Git;

import com.salescode.bean.UserBean;
import com.salescode.form.RepositoryForm;
import com.salescode.entity.Repository;
import com.salescode.entity.User;
import com.salescode.bean.RepositoryBean;

/**
 * Servlet implementation class Issues
 */
@WebServlet("/repositories")
public class Repositories extends HttpServlet {
	private static final long serialVersionUID = 1L;
	public static final String USER_SESSION_NAME = "user"; // l'objet user en session sera accessible via sessionScope.user.
	private static final String ROOT_DIRECTORY = "C:/temp";

	@EJB
    UserBean userBean;
	@EJB
	RepositoryBean repositoryBean = new RepositoryBean();
	
    public Repositories() {
        super();
    }

    public void doGet( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException {
    	String creator = request.getParameter("creator");
    	String op = request.getParameter("op").trim();
    	String id = request.getParameter("id");
    	HttpSession session = request.getSession();
    	
    	//on essaie de charger l'objet que l'on attend
    	User userCreator = null;
    	Repository repo = null;
    	try{
    		userCreator= userBean.findByUsername(creator);
    		repo = repositoryBean.findByTitleCreator(userCreator, op);
    	}catch(NullPointerException e){}
    	
    	
    	if(op != null && op.equals("new") && session.getAttribute("user") != null ){//affichage du form pour créer un nouveau repo.
            this.getServletContext().getRequestDispatcher( "/WEB-INF/views/repository/new.jsp" ).forward( request, response );
    	}
    	else if(creator != null && op != null && userCreator != null && repo != null ){//affichage d'un repository

    		request.setAttribute("repository", repo);
            this.getServletContext().getRequestDispatcher( "/WEB-INF/views/repository/repository.jsp" ).forward( request, response );

    	}
    	else if(op != null && op.equals("list")){
    		System.out.println("TO DO : inclure list");
    	}
    	else if(session.getAttribute("user") == null){
    		String message = "erreur de connexion";
    		response.sendRedirect( "/salescode/connexion" );
    	}
    	
    	
    }

    public void doPost( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException {
    	/** Envoi d'une requete de creation d'un repository.*/
    	String op = request.getParameter("op");
    	String id = request.getParameter("id");

    	String message = null;//variable de message a afficher en réponse à l'action.
    	HttpSession session = request.getSession();
    	
    	if(session.getAttribute( "user" ) != null ){//s'il est connecté
	    	
    		if(op != null && op.equals("new")){
    			postNew( request, response );
	    	}

    	}
    	else{
    		message = "erreur de connexion";
    		response.sendRedirect( "/salescode/connexion" );
    	}

    	//ajout du message dans les attributs, retourne un json contenant le résultat pour trtaitement ultérieur par Jquery.

    	
    	
    	
    }
    
    
    //fonction pour creer un nouveau dépot.
    private void postNew( HttpServletRequest request, HttpServletResponse response ) throws IOException, ServletException{
    	String message = null;//variable de message a afficher en réponse à l'action.
    	HttpSession session = request.getSession();
		 /* Préparation de l'objet formulaire */
       RepositoryForm form = new RepositoryForm();
		
       /* Appel au traitement et à la validation de la requête, et récupération du bean en r�sultant */
       Repository repository=null;
		try {
			repository = form.registerRepository( request );
			if(repository != null){
				File repoFile = new File(ROOT_DIRECTORY+"/"+repository.getCreator().getUsername()+"/"+repository.getTitle());

		        // create the directory
		        try (Git git = Git.init().setDirectory(repoFile).call()) {
		            System.out.println("Having repository: " + git.getRepository().getDirectory());
		        }
				repositoryBean.save(repository);
			}
			else{
				message = "formulaire invalide.";
			}
		} catch (Exception e) {message = "Erreur de création de repository.";		}
		
       /* Stockage du formulaire et du bean dans l'objet request */
       request.setAttribute( "form", form );
       request.setAttribute( "repository", repository );
		
       //si la creation a réussie : on redirige vers la vue du tout nouveau depot
       if(repository != null){
       	response.sendRedirect( "/salescode/r/"+repository.getCreator().getUsername()+"/"+repository.getTitle()+"/" );
       }
       else{
           //sinon on renvoie la page de creation avec les erreurs.
           this.getServletContext().getRequestDispatcher( "/WEB-INF/views/repository/new.jsp" ).forward( request, response );
       }
   	
    }

}
