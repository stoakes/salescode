package com.salescode.servlet;

import java.io.IOException;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.salescode.bean.UserBean;
import com.salescode.entity.ShoppingCart;
import com.salescode.entity.User;
import com.salescode.form.ConnexionForm;

/**
 * Servlet implementation class Connexion
 */
@WebServlet("/connexion")
public class Connexion extends HttpServlet {
	private static final long serialVersionUID = 1L;
    
	public final String VUE = "/WEB-INF/views/connexion.jsp"; //la vue de référence de la servelt. Public pour permettre aux autres controllers d'y faire référence sans connaitre le chemin précis.
	public static final String USER_SESSION_NAME = "user"; // l'objet user en session sera accessible via sessionScope.user.
	public static final String SHOPPING_CART_SESSION="shopCart"; //Panier de l'utilisateur
	private static final String URL_SUCCES = "/salescode/dashboard"; //l'url vers ou rediriger en cas de succès de la connexion
	private static final String URL_ECHEC = "/salescode/connexion"; //l'url vers ou rediriger en cas de echec de la connexion
	

	@EJB
    UserBean userBean;
	
    public Connexion() {
        super();
    }

    public void doGet( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException {
    	//il n'est pas connecté, on lui affiche la page de connexion
    	if(request.getParameter("op") == null && request.getSession().getAttribute("user") == null){ 
    		this.getServletContext().getRequestDispatcher( this.VUE ).forward( request, response );
    	}
    	//connecté et pas d'opérateur, il vient ici par erreur, on le renvoie sur le dashboard
    	else if( request.getParameter("op") == null && request.getSession().getAttribute("user") != null ){
    		response.sendRedirect( URL_SUCCES );
    	}
    	else{//op est set, donc il veut se déconnecter
    		HttpSession session = request.getSession();
            session.invalidate();

            /* Redirection vers la page d'accueil ! */
            response.sendRedirect( "/salescode/" );
    	}
    }

    public void doPost( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException {
        /* Préparation de l'objet formulaire */
        ConnexionForm form = new ConnexionForm();

        /* Traitement de la requête et récupération du bean en résultant */
        User user = form.connexion( request );

        /* Récupération de la session depuis la requête */
        HttpSession session = request.getSession();
        
        String message = null;
        if ( form.getErrors().isEmpty() ) {
            //on récupere l'utilisateur qui aurait le mail saisi.
        	User userTest;
        	userTest = userBean.findByEmail(user.getEmail());
           
        	if(message == null ){ // uniquement s'il n'y a pas eu d'erreur auparavant
        		if(userTest != null){
		            if(userTest.isEnabled()){ // s'il a activé son compte
		            	if(!userTest.isLocked()){ // s'il n'est pas banni
			            	try{
			          
				            //on compare son mot de passe avec le hash de celui saisi :
				            if( userTest.getPassword().equals(  user.hash(userTest.getSalt(), user.getPassword())  ) ){ // il a le bon mot de passe
				                /**
				                 * Si aucune erreur de validation n'a eu lieu, alors ajout du bean
				                 * Utilisateur à la session, sinon suppression du bean de la session.
				                 */
				                    session.setAttribute( USER_SESSION_NAME, userTest );
				                    ShoppingCart shoppingCart = (ShoppingCart)session.getAttribute(SHOPPING_CART_SESSION);
				                    if (shoppingCart==null){
				                        shoppingCart = new ShoppingCart();
				                        session.setAttribute(SHOPPING_CART_SESSION,shoppingCart);
				                    }
				                    message = "Vous etes bien connecté.";
				                } else {
				            		message = "Votre couple email/mot de passe est faux"; //message volontairement vague. 
				                    session.setAttribute( USER_SESSION_NAME, null );
				                }
				            }
			            	catch(Exception e){ message = e.getMessage(); }
		            	}
		            	else{ message = "Vous êtes banni."; }
		            }
		            else{ message = "Vous n'êtes pas activé. Cliquez sur le lien d'activation dans votre mail."; }
        		}
        		else{ message = "Ce compte n'existe pas."; }
        	}
        }
        else{ message = "Erreur(s) dans le formulaire"; }


        /* Stockage du formulaire et du bean dans l'objet request */
        request.setAttribute( "form", form );
        request.setAttribute( "user", user );
        request.setAttribute( "message", message );
        
        //selon les cas, on redirige vers le dashboard, ou on renvoie le formulaire de connexion avec les messages.
        if(session.getAttribute(USER_SESSION_NAME) != null){
        	response.sendRedirect( URL_SUCCES );
        }
        else{
        	this.getServletContext().getRequestDispatcher( this.VUE).forward( request, response );
        }
    }

}
