package com.salescode.servlet;

import java.io.IOException;
import java.util.List;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.salescode.bean.RepositoryBean;
import com.salescode.entity.Repository;




@WebServlet("/marketplace")
public class ECommerce extends HttpServlet {
	public static final String VUE = "/WEB-INF/views/ecommerce/list.jsp";
	public static final String PRO = "/WEB-INF/views/r/";
	private static final long serialVersionUID = 1L;
	@EJB
	RepositoryBean rManager;   
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ECommerce() {
        super();
        // TODO Auto-generated constructor stub
    }

    public void doGet( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException{
        /* Affichage de la liste des repos */
    	 int page = 1;
         int productPerPage= 3;
         if(request.getParameter("page") != null)
             page = Integer.parseInt(request.getParameter("page"));
         List<Repository> listRepo = rManager.retrieveAllECommerceRepositories();
         int repoCount = listRepo.size();
         int pageNumber=(int) Math.ceil(repoCount * 1.0 / productPerPage);
         int offset=(page-1)*productPerPage+3;
         if (repoCount-(page-1)*productPerPage<3){
        	 offset=(page-1)*productPerPage+(repoCount-(page-1)*productPerPage);
         }
    	request.setAttribute("listRepo", listRepo.subList((page-1)*productPerPage, offset));
        request.setAttribute("pageNumber", pageNumber);
        request.setAttribute("currentPage", page);
        if (request.getParameter("p")!=null){
        	int id=Integer.parseInt(request.getParameter("p"));
        	Repository r=rManager.findById(id);
        	request.setAttribute("repo", rManager.findById(id));
        	response.sendRedirect("r/"+r.getCreator().getUsername()+"/"+r.getTitle()+"/");
        	
        } else {
        this.getServletContext().getRequestDispatcher( VUE ).forward( request, response );
        }
    }
	
    public void doPost( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException{
		
        this.getServletContext().getRequestDispatcher( VUE ).forward( request, response );
    }
}
