package com.salescode.servlet;

import java.io.IOException;
import java.util.List;
import java.util.ListIterator;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.salescode.bean.BillBean;
import com.salescode.bean.RepositoryBean;
import com.salescode.bean.UserBean;
import com.salescode.entity.Bill;
import com.salescode.entity.Repository;
import com.salescode.entity.User;

@WebServlet("/dashboard")
public class Dashboard extends HttpServlet {
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public static final String PUBLIC_ACCESS     = "/";
    public static final String RESTRICTED_ACCESS  = "/WEB-INF/views/dashboard.jsp";
    public static final String USER_SESSION = "user";
    @EJB
    RepositoryBean rManager;
    @EJB
    UserBean uManager;
    @EJB
    BillBean billBean;
    
    
    public void doGet( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException {
        /* Récupération de la session depuis la requête */
        HttpSession session = request.getSession();

        /*
         * Si l'objet utilisateur n'existe pas dans la session en cours, alors
         * l'utilisateur n'est pas connecté.
         */
        if ( session.getAttribute( USER_SESSION ) == null ) {//s'il n'est pas connecté
            /* Redirection vers la page publique */
            response.sendRedirect( request.getContextPath() + PUBLIC_ACCESS );
        } else {
            /** affichage du dashboard **/
        	User u=(User) session.getAttribute("user");
        	List<Repository> persoRepositories=rManager.findByCreator((User) session.getAttribute("user"));
        	List<Bill> listBill=billBean.findByCreator(u);
        	ListIterator<Bill> bitr =listBill.listIterator();
        	while(bitr.hasNext()){
        		persoRepositories.add(bitr.next().getRepository());
        	}
        	request.setAttribute("persoRepositories", persoRepositories);
        	
            this.getServletContext().getRequestDispatcher( RESTRICTED_ACCESS ).forward( request, response );
        }
    }
}

