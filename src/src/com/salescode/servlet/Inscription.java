package com.salescode.servlet;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.salescode.bean.UserBean;
import com.salescode.entity.User;
import com.salescode.form.InscriptionForm;


@WebServlet("/inscription")
public class Inscription extends HttpServlet {
	
	private static final long serialVersionUID = -9035269629210247806L;
	public static final String VUE = "/WEB-INF/views/inscription.jsp";
    public static final String EMAIL_FIELD = "email";
    public static final String PASSWORD_FIELD = "password";
    public static final String VERIF_FIELD = "confirmation";
    public static final String USERNAME_FIELD = "nom";
    
    @EJB
    UserBean userbean;
    

    public void doGet( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException{
       HttpSession session = request.getSession();
       
       if(session.getAttribute("user") == null){//si pas connecté : affichage page d'inscription
    	/* Affichage de la page d'inscription */
        this.getServletContext().getRequestDispatcher( VUE ).forward( request, response );
       }
       else{//redirection vers dashboard
    	   response.sendRedirect("/salescode/dashboard");
       }
    }
	
    public void doPost( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException{
        /* Préparation de l'objet formulaire */
        InscriptionForm form = new InscriptionForm(userbean);
        String message = "";
		
        /* Appel au traitement et à la validation de la requête, et récupération du bean en résultant */
        User user = null;
		try {
			user = form.registerUser( request );
			if(user != null && form.getErrors().isEmpty()){ //pas d'erreurs
				userbean.save(user);
				message = "Votre inscription est achevée.";
			}
		} catch (NoSuchAlgorithmException e) { message = "erreur interne";}
		
        /* Stockage du formulaire et du bean dans l'objet request */
		request.setAttribute( "message", message );
        request.setAttribute( "form", form );
        request.setAttribute( "user", user );
		
        this.getServletContext().getRequestDispatcher( VUE ).forward( request, response );
    }
}