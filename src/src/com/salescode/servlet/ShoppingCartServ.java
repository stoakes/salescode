package com.salescode.servlet;

import java.io.IOException;
import java.net.URL;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Set;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.salescode.bean.BillBean;
import com.salescode.bean.RepositoryBean;
import com.salescode.entity.Bill;
import com.salescode.entity.Purchase;
import com.salescode.entity.Repository;
import com.salescode.entity.ShoppingCart;
import com.salescode.entity.User;
import com.salescode.misc.Paypal;

/**
 * Servlet implementation class ShoppingCart
 */
@WebServlet("/ShoppingCart")
public class ShoppingCartServ extends HttpServlet {
	private static final long serialVersionUID = 1L;
	public static final String CONNEXION = "/WEB-INF/views/connexion.jsp";
	public static final String VUE = "/WEB-INF/views/ecommerce/shoppingCart.jsp";
	public static final String PAY = "/WEB-INF/views/ecommerce/paypalpay.jsp";
	public static final String LIST = "/WEB-INF/views/ecommerce/list.jsp";
	public static final String SHOPPING_CART_SESSION = "shopCart";   
    
	@EJB
	RepositoryBean repositoryBean;
	@EJB
	BillBean billBean;
	/**
     * @see HttpServlet#HttpServlet()
     */
    public ShoppingCartServ() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		//Test si le user est connect� sinon go page de connexion
		
		if(request.getParameter("op") == null && request.getSession().getAttribute("user") == null){ 
			
    		this.getServletContext().getRequestDispatcher( ShoppingCartServ.CONNEXION ).forward( request, response );
    	}
		
		//Ajout du produit au panier
		ShoppingCart shoppingCart = (ShoppingCart)session.getAttribute(SHOPPING_CART_SESSION);
		
		if (request.getParameter("product")!=null){
			int productId =Integer.parseInt(request.getParameter("product"));
			Repository r=repositoryBean.findById(productId);
			User u=(User)session.getAttribute("user");
			List<Bill> listBill=billBean.findByCreator(u);
			ListIterator<Bill> bitr =listBill.listIterator();
			int purchased = 0;
	        while(bitr.hasNext()){
	    
	         	if(bitr.next().getRepository().getId()==productId){
	         		purchased=1;
	         	}
	        }
			if(r.getCreator().getId()!=u.getId()&&purchased!=1){
				Purchase purchase=new Purchase(repositoryBean.findById(productId));
				shoppingCart.addRepository(purchase);
			}else if(purchased==1) {
				request.setAttribute("purchased",1);
				this.getServletContext().getRequestDispatcher( ShoppingCartServ.VUE ).forward( request, response );
			}else{
				request.setAttribute("yet",1);
				this.getServletContext().getRequestDispatcher( ShoppingCartServ.VUE ).forward( request, response );
			}
		}
		if(shoppingCart.getListPurchase().isEmpty()){
			request.setAttribute("empt", 1);
			this.getServletContext().getRequestDispatcher( ShoppingCartServ.VUE ).forward( request, response );
		}
		//Passage du panier du user � la page
		request.setAttribute("listPurchase", shoppingCart.getListPurchase());
		int cancel=0;
		int success=0;
		//Affichage panier
		if(request.getParameter("rm")==null && request.getParameter("valid")==null&& request.getParameter("token")==null&& request.getParameter("PayerID")==null){
			this.getServletContext().getRequestDispatcher( ShoppingCartServ.VUE ).forward( request, response );
			
		}else if(request.getParameter("valid")==null&& request.getParameter("token")==null&& request.getParameter("PayerID")==null){//Suppression produit panier
			int rmId=Integer.parseInt(request.getParameter("rm"));
			shoppingCart.removeRepository(new Purchase(repositoryBean.findById(rmId)));
			request.setAttribute("listPurchase", shoppingCart.getListPurchase());
			this.getServletContext().getRequestDispatcher( ShoppingCartServ.VUE ).forward( request, response );
		} else if (request.getParameter("token")==null&& request.getParameter("PayerID")==null){//Configuration Url Paypal
			Paypal pay=new Paypal();
			pay.recoverParameter(pay.constructUrl(shoppingCart.getPrice()));
			response.sendRedirect(pay.redirectPaiement());
		} else if (request.getParameter("PayerID")==null){//Paiement annul�
			cancel=1;
			request.setAttribute("cancel", cancel);
			this.getServletContext().getRequestDispatcher( ShoppingCartServ.VUE ).forward( request, response );
		} else {//Paiement valid�
			Paypal pay=new Paypal();
			String urlSuccess=pay.paiementSucess(request.getParameter("token"), request.getParameter("PayerID"), shoppingCart.getPrice());
			URL url=new URL(urlSuccess);
			url.openStream();
			 int i=1;
			 User u=(User) session.getAttribute("user");
			 Calendar calendar = Calendar.getInstance();
			 //On it�re sur le panier
			 Set<Integer> keys = shoppingCart.getListPurchase().keySet();
			 Iterator<Integer> itr = keys.iterator();
		      while(itr.hasNext()) {
		    	  Repository repo =shoppingCart.getListPurchase().get(itr.next()).getRepository();
		    	  repo.addSales();
		    	  Bill bill=new Bill();		    	  
		    	  bill.setNumero("F"+calendar.get(Calendar.YEAR)+"-"+u.getId()+"-"+repo.getId());
		    	  bill.setUserTo(u);
		    	  bill.setRepository(repo);
		    	  bill.setPrice(repo.getPrice());
		    	  bill.setCreatedOn(new Timestamp(new java.util.Date().getTime()));
		    	  System.out.println(bill.getNumero());
		    	  billBean.save(bill);
		    	  repositoryBean.merge(repo);
		    	  
		      }
		    shoppingCart.getListPurchase().clear();
		    success=1;
		    
			request.setAttribute("success", success);
			this.getServletContext().getRequestDispatcher( ShoppingCartServ.LIST ).forward( request, response );
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
