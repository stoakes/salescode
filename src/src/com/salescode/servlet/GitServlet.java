package com.salescode.servlet;

import java.io.File;
import java.io.IOException;
import java.util.List;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.api.errors.NoHeadException;
import org.eclipse.jgit.revwalk.RevCommit;
import org.eclipse.jgit.storage.file.FileRepositoryBuilder;
import com.salescode.bean.RepositoryBean;
import com.salescode.bean.UserBean;
import com.salescode.entity.Repository;

/**
 * Servlet implementation class Git
 */
@WebServlet("/Git")
public class GitServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	@EJB
	RepositoryBean repositoryBean;
	@EJB
	UserBean userBean;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public GitServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * Retourne des bouts d'html pour des affichages asynchrone
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String op =request.getParameter("op");
		String creator =request.getParameter("creator");
		String action =request.getParameter("action");
		String folder = request.getParameter("folder");
		
		String message = null;
		Repository repository = null;
		if(op != null && creator != null){//si les parametres de depots sont valides
			repository = repositoryBean.findByTitleCreator(userBean.findByUsername(creator),op );
		}
		else{message = "parametres incorrects"; }
		
		if(repository != null){//si le depot existe
			request.setAttribute("repository", repository);
			if(action != null && action.equals("listCommit")){//est ce que l'on veut afficher la liste des commits
				try{
					getListCommit(request, response);
				}
				catch(Exception e){ message = e.getMessage(); }
			}
			else if(folder != null){ //on veut afficher un fichier
				getFolderContent(request, response);
			}
			else{
				message = "action inconnue";
			}
		}else{ message = "Depot inexistant"; }
		
		if(message != null){ //uniquement s'il y a eu une erreur, evite de lever une exception de sécurité sur un forward après un premier rendu.
			request.setAttribute("message", message);
			this.getServletContext().getRequestDispatcher( "/WEB-INF/views/git/error.jsp" ).forward( request, response );
		}
		
	}

	/**	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}
	
	private void getFolderContent(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		com.salescode.entity.Repository eRepo = (Repository) request.getAttribute("repository");
		String folder = request.getParameter("folder");

		String message = null;
		String fileURI = "C:/temp/"+eRepo.getCreator().getUsername()+"/"+eRepo.getTitle()+"/";
		fileURI += folder.replace("..", "");
		File testFile = null;
		File[] fileList = null;
		try{
			testFile = new File(fileURI);
		}
		catch(Exception e){
			e.printStackTrace();
		}
		
		if(testFile.exists()){ // si le fichier existe bien
			if(testFile.isDirectory()){//si c'est un dossier, on affiche le contenu
				 fileList = listf(fileURI);
			}else{//on est sur un fichier raw, on le retourne.
				
			}
		}
		else{
			message = "fichier inconnu";
		}
		
		request.setAttribute("message", message);
		request.setAttribute("fileList", fileList);
		this.getServletContext().getRequestDispatcher( "/WEB-INF/views/git/folder.jsp" ).forward( request, response );
	}
	
	private File[] listf(String directoryName) {

	    File directory = new File(directoryName);

	    // get all the files from a directory
	    File[] fList = directory.listFiles();

	    for (File file : fList) {
	        if (file.isFile()) {
	            /**
	             * StringBuilder sb = new StringBuilder();
        
        try {
            URLConnection connection = (new URL(fileUrl)).openConnection();
            connection.setConnectTimeout(10000);
            connection.setReadTimeout(10000);
            connection.connect();
            
            Scanner s = new Scanner(connection.getInputStream());
            
            while (s.hasNextLine()) {
                sb.append(s.nextLine() + "\n");
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        
        return sb.toString();
	             */
	        } else if (file.isDirectory()) {
	            //listf(file.getAbsolutePath());
	        }
	    }
	    return fList;
	}  
	
	
	
	
	private void getListCommit(HttpServletRequest request, HttpServletResponse response) throws IOException, NoHeadException, GitAPIException, ServletException{
		com.salescode.entity.Repository eRepo = (Repository) request.getAttribute("repository");

		File gitWorkDir = new File("C:/temp/"+eRepo.getCreator().getUsername()+"/"+eRepo.getTitle()+"/");
	    Git git = Git.open(gitWorkDir);
	    org.eclipse.jgit.lib.Repository repo = git.getRepository();
	    Iterable<RevCommit> commits = git.log().all().call();
	    
	    request.setAttribute("listCommit", commits);
		this.getServletContext().getRequestDispatcher( "/WEB-INF/views/git/listCommit.jsp" ).forward( request, response );
            

	}

}
