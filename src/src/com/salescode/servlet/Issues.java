package com.salescode.servlet;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;

import javax.ejb.EJB;
import javax.persistence.NoResultException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.salescode.bean.UserBean;
import com.salescode.form.IssuesForm;
import com.salescode.entity.Issue;
import com.salescode.entity.IssueCommentary;
import com.google.gson.Gson;
import com.salescode.bean.IssueBean;
import com.salescode.bean.NotificationBean;
import com.salescode.bean.RepositoryBean;
import com.salescode.entity.Repository;
import com.salescode.entity.User;

/**
 * Servlet implementation class Issues
 */
@WebServlet("/issues")
public class Issues extends HttpServlet {
	private static final long serialVersionUID = 1L;
	public static final String VUE = "/WEB-INF/views/connexion.jsp"; //la vue de référence de la servelt. Public pour permettre aux autres controllers d'y faire référence sans connaitre le chemin précis.
	public static final String USER_SESSION_NAME = "user"; // l'objet user en session sera accessible via sessionScope.user.

	@EJB
	UserBean userBean;
	@EJB
	IssueBean issueBean;
	@EJB
	RepositoryBean repositoryBean;
	@EJB
	NotificationBean notificationBean;
	
    public Issues() {
        super();
    }

    //format des routes d'issues : <nom depot>/issues/ la liste de tt les issues.
    //								<nom depot>/issues/<id> un issue et ses commentaires.
    public void doGet( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException {
    	String creator = (String) request.getParameter("creator");
    	String op = (String) request.getParameter("op");
    	String id = (String) request.getParameter("id");

    	
    	if(op != null && !op.equals("") && creator != null && !creator.equals("") ){
	    	Repository repo = repositoryBean.findByTitleCreator(userBean.findByUsername(creator),op);
	    	if(repo != null){// si le repertoire existe bien.
	    		if(id == null || id.equals("")){ // si l'id de l'issue n'est pas défini, on affiche toute la liste
	    			
	    			List<Issue> issueList = issueBean.findByRepository(repo);
	    			request.setAttribute("repository", repo);
	    			request.setAttribute("issueList", issueList);
	    			//on passe la liste et le repo en paramètres
	    			this.getServletContext().getRequestDispatcher("/WEB-INF/views/issue/list.jsp").forward( request, response );
	    		}
	    		else if(id.equals("new")){//afichage du form pour un new issue
	    			if(request.getSession().getAttribute("user") != null){//s'il est connecté : on lui propose le form
	    				this.getServletContext().getRequestDispatcher("/WEB-INF/views/issue/new.jsp").forward( request, response );
	    			}
	    			else{//non connecté : redirection vers la page de connexion
	    				response.sendRedirect("/salescode/connexion");
	    			}
	    		}
	    		else{
	    			Issue issue = null;
					try{
						issue = issueBean.findById(Integer.parseInt(id));
					}
	    			catch(NoResultException e){}// catch les erreurs si aucun issue n'existe.
					
	    			if(issue != null && issue.getRepository().getId() == repo.getId()){ // si l'issue existe et qu'on l'affiche en passant par la bonne arborescence (cohérence entre repository demandé et celui lié à l'issue)
	    				
	    				request.setAttribute("issue", issue);
	    				this.getServletContext().getRequestDispatcher("/WEB-INF/views/issue/issue.jsp").forward( request, response );
	    			}
	    			else{//404
	    	        	this.getServletContext().getRequestDispatcher("/WEB-INF/views/404.jsp").forward( request, response );
	    			}
	    		}
	    	}else{ this.getServletContext().getRequestDispatcher("/WEB-INF/views/404.jsp").forward( request, response ); }
    	}
    	else{ this.getServletContext().getRequestDispatcher("/WEB-INF/views/404.jsp").forward( request, response ); }
  
    	
        	
    		   	
    	
    }
    
    /**
     * Interface API :
     * Un tableau JSON avec :
     * 		un champ message : le/les messages d'erreurs
     * 		un champ message-class : la classe du container. (reference Bootstrap)
     */
    public void doPost( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException {
       /** Envoi d'une action sur un issue**/
    	
    	HashMap<String, Object> result = new HashMap<String, Object>();//La hashmap d'api. Sera retournée en JSON.
    	HttpSession session = request.getSession();
    	
    	if(session.getAttribute( "user" ) != null ){
    		
    		String action = (String) request.getParameter("action");

    		if(action != null && action.equals("new") && request.getParameter("message") != null && request.getParameter("title") != null){//nouvelle issue
	    		
	    		IssuesForm issueForm = new IssuesForm(userBean,issueBean,repositoryBean);
	    		Issue i = issueForm.registerNewIssue(request);
	    		if(i != null){    		
	    			issueBean.save(i);//persistence.
	    			result.put("message","Issue créé.");
	        		result.put("message-class","success");
	        		
	        		//on notifie le créateur du depot si ce n'est pas lui qui l'a ouverte :
	        		if(((User)session.getAttribute("user")).getId() != i.getRepository().getCreator().getId()){
	        			String url = "/salescode/r/"+i.getRepository().getCreator().getUsername()+"/"+i.getRepository().getTitle()+"/issues/"+i.getId();//l'url de visionnage de l'issue;
	        		notificationBean.newNotification("new issue", 
	        				i.getRepository().getCreator(),
	        				"Nouvel issue sur "+i.getRepository().getTitle(),
	        				i.getCreator().getUsername()+" vient d'ouvrir un nouvel issue : <a href=\""+url+"\">"+i.getTitle()+"</a>");
	        		}
	    		}
	    		else{ 
	    			result.put("message",issueForm.getErreurs() );
	        		result.put("message-class","warning");
	    		}
	    	}
	    	else if(action != null && action.equals("post") & request.getParameter("id") != null ){//nouveau message sur cet issue
	    		IssuesForm issueForm = new IssuesForm(userBean,issueBean,repositoryBean);
	    		IssueCommentary iC = issueForm.registerNewIssueCommentary(request);
	    		if(iC != null){    		
	    			issueBean.saveCommentary(iC);//persistence.
	    			result.put("message","Commentaire ajouté");
	        		result.put("message-class","success");
	    		}
	    	}
    		else if(action != null && action.equals("close") & request.getParameter("id") != null ){//fermeture de l'issue.
	    		
    			if(request.getParameter("id") != null){
    				
		    		Issue i = issueBean.findById(Integer.parseInt(request.getParameter("id")));
		    		if(i != null){ 
		    			//verification que la personne essayant de clore l'issue est soit le propriétaire du dépot, soit le createur de l'issue
		    			User user = (User) session.getAttribute("user");
		    			if(user.getId() == i.getCreator().getId() || user.getId() == i.getRepository().getCreator().getId()){
			    			i.setClosed(true);
			    			i.setClosedOn(new Timestamp(new java.util.Date().getTime()));
			    			i.setCloseUser(user);
			    			issueBean.mergeIssue(i);//persistence.
			    			result.put("message","Issue fermé.");
			        		result.put("message-class","success");
		    			}
		    			else{
		    				result.put("message","Un issue ne peut être clot que par son créateur ou le propriétaire du depot.");
			        		result.put("message-class","danger");
		    			}
		    		}
		    		else{
		    			result.put("message","Id issue invalideu" );
			    		result.put("message-class","danger");
		    		}
    			}
	    	}
	    	else{ 
				result.put("message","Appel inconnu" );
	    		result.put("message-class","warning");
			}
		}
		else{
			result.put("message","Vous n'êtes plus connecté.");
			result.put("message-class","danger");
		}
    	
    	//L'objet est casté en JSON
        String json = new Gson().toJson(result);

        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        response.getWriter().write(json);
    	
    }

	public UserBean getUserBean() {
		return userBean;
	}

	public void setUserBean(UserBean userBean) {
		this.userBean = userBean;
	}

	public IssueBean getIssueBean() {
		return issueBean;
	}

	public void setIssueBean(IssueBean issueBean) {
		this.issueBean = issueBean;
	}

	public RepositoryBean getRepositoryBean() {
		return repositoryBean;
	}

	public void setRepositoryBean(RepositoryBean repositoryBean) {
		this.repositoryBean = repositoryBean;
	}
    
    
    
    
    
    

}
