package com.salescode.servlet;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import javax.ejb.EJB;
import javax.persistence.NoResultException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.salescode.bean.UserBean;
import com.salescode.form.IssuesForm;
import com.salescode.entity.Issue;
import com.salescode.entity.IssueCommentary;
import com.google.gson.Gson;
import com.salescode.bean.IssueBean;
import com.salescode.bean.NotificationBean;
import com.salescode.bean.RepositoryBean;
import com.salescode.entity.Repository;
import com.salescode.entity.User;

/**
 * Servlet implementation class Issues
 */
@WebServlet("/notification")
public class Notification extends HttpServlet {
	private static final long serialVersionUID = 1L;
	//aucune vue, on ne fait que proposer des points d'acces

	@EJB
    NotificationBean notificationBean;
	
    public Notification() {
        super();
    }

    /**
     * DoGet : count
     * list all
     * read all.
     */
    public void doGet( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException {
    	String action = request.getParameter("action");
    	HttpSession session = request.getSession();
    	User user = (User) session.getAttribute("user");
     	HashMap<String, Object> result = new HashMap<String, Object>();//La hashmap d'api. Sera retournée en JSON.
    	
    	if(user != null){
    	
	    	if(action != null  && action.equals("count")){
	    		int numberAvailable = notificationBean.countUnreadNotif(user);
	    		result.put("message",numberAvailable);
	    		result.put("message-class","success");
	    	}
	    	else if(action != null  && action.equals("listAll")){
	    		result.put("message", notificationBean.findByUserToUnRead(user));
	    		result.put("message-class", "success");
	    	}
	    	else if(action != null  && action.equals("readAll")){
	    		notificationBean.readNotif(user);
	    		result.put("message", notificationBean.countUnreadNotif(user));
	    		result.put("message-class", "success");
	    	}
	    	else{
	    		result.put("message","Appel API inconnu. Appels disponibles : count, listAll, readAll");
	    		result.put("message-class","danger");
	    	}
	    	
    	}
    	else{
     		result.put("message","Vous n'êtes plus connecté.");
     		result.put("message-class","danger");
     	}
    	    	
     	
     	//L'objet est casté en JSON
         String json = new Gson().toJson(result);

         response.setContentType("application/json");
         response.setCharacterEncoding("UTF-8");
         response.getWriter().write(json);

    		   	
    	
    }
    
    /**
     * Interface API :
     * Un tableau JSON avec :
     * 		un champ message : le/les messages d'erreurs
     * 		un champ message-class : la classe du container. (reference Bootstrap)
     */
    public void doPost( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException {  	
    	    	
    }

}
