package com.salescode.servlet;

public class Erreur500Exception extends Exception {

	public Erreur500Exception(String message){
		super(message);
	}
}
