package com.salescode.servlet;

import java.io.IOException;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.salescode.bean.UserBean;
import com.salescode.entity.User;

/**
 * Servlet implementation class EspaceAdmin
 */
@WebServlet("/EspaceAdmin")
public class EspaceAdmin extends HttpServlet {

	public static final String VUE_LIST = "/WEB-INF/views/espaceAdmin/list.jsp";
	public static final String VUE_EDIT = "/WEB-INF/views/espaceAdmin/edit.jsp";
	@EJB
	UserBean userbean;
	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public EspaceAdmin() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		User u = (User) session.getAttribute("user");
		if( u == null){//si pas connecte : redirection vers connexion
			response.sendRedirect("/salescode/connexion");
		} else {
			//on verifie que ca soit un moderateur ou l'administrateur
			if (u.getRoles() < 49){// si ce n'est pas un administrateur ou moderateur
				response.sendRedirect("/salescode/dashboard");
			} else {//c'est un administrateur ou moderateur
				//cas de traitement sur l'operatuer id 

				String stid = request.getParameter("id");
				if(stid != null){
					int id = Integer.parseInt(stid);
					User user = userbean.findById(id);
					request.setAttribute("user",  user);

					this.getServletContext().getRequestDispatcher( VUE_EDIT ).forward( request, response );

				} else {//on affiche un utilisateur en particulier
					java.util.List<User> userlist;
					userlist = userbean.retrieveAllUser();
					request.setAttribute("userList", userlist);
					this.getServletContext().getRequestDispatcher( VUE_LIST ).forward( request, response );
				}

			}
		}
	}


	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		User u = (User) session.getAttribute("user");
		if( u == null){//si pas connecte : redirection vers connexion
			response.sendRedirect("/salescode/connexion");
		} else {//il est connecte
			boolean enabled;
			boolean locked;
			
			String stenabled = request.getParameter("enabled");
			if(stenabled != null){
				int ienabled = Integer.parseInt(stenabled);
				if(ienabled == 1){
					enabled = true;
					u.setEnabled(enabled);
				} else {
					enabled = false;
					u.setEnabled(enabled);
				}
			} 
			String stlocked = request.getParameter("locked");
			if(stlocked != null){
				int ilocked = Integer.parseInt(stlocked);
				if(ilocked == 1){
					locked = true;
					u.setLocked(locked);
				} else {
					locked = false;
					u.setLocked(locked);
				}
			}

			userbean.merge(u);
			this.getServletContext().getRequestDispatcher( VUE_LIST ).forward( request, response );
			
		}
	}

}
