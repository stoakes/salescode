package com.salescode.servlet;

import java.io.IOException;
import java.util.List;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.salescode.bean.BillBean;
import com.salescode.bean.RepositoryBean;
import com.salescode.entity.Bill;
import com.salescode.entity.User;

/**
 * Servlet implementation class ShoppingCart
 */
@WebServlet("/bill")
public class BillServ extends HttpServlet {
	private static final long serialVersionUID = 1L;
	public static final String CONNEXION = "/WEB-INF/views/connexion.jsp";
	public static final String VUE = "/WEB-INF/views/ecommerce/bill.jsp";
	public static final String BIL = "/WEB-INF/views/ecommerce/billDisplay.jsp";
    
	@EJB
	RepositoryBean repositoryBean;
	@EJB
	BillBean billBean;
	/**
     * @see HttpServlet#HttpServlet()
     */
    public BillServ() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		//Test si le user est connect� sinon go page de connexion
		User u = (User) session.getAttribute("user");
		List<Bill> listBill=billBean.findByCreator(u);
		
		if(request.getSession().getAttribute("user") == null){ 
			
    		this.getServletContext().getRequestDispatcher( BillServ.CONNEXION ).forward( request, response );
    	}
		
		if(request.getParameter("nb")!=null){
			int id=Integer.parseInt(request.getParameter("nb"));
			Bill bill=billBean.findById(id);
			if(bill.getUserTo().getId()==u.getId()){
				request.setAttribute("repo", bill.getRepository());
				request.setAttribute("bill", bill);
				request.setAttribute("user", u);
				this.getServletContext().getRequestDispatcher( BillServ.BIL ).forward( request, response );
			} else {
				request.setAttribute("listBill", listBill);
				this.getServletContext().getRequestDispatcher( BillServ.VUE ).forward( request, response );
			}
		} else {
		
		request.setAttribute("listBill", listBill);
		this.getServletContext().getRequestDispatcher( BillServ.VUE ).forward( request, response );
		}
	}
	

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
