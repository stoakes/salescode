package com.salescode.entity;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Timestamp;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
 
@Entity
@Table(
	    uniqueConstraints={
	        @UniqueConstraint(columnNames={"username"}),
	        @UniqueConstraint(columnNames={"usernameCanonical"}),
	        @UniqueConstraint(columnNames={"email"}),
	        @UniqueConstraint(columnNames={"emailCanonical"})
	    }
	)

public class User implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private int id;
	private String username;
	private String usernameCanonical; // le nom d'utilisateur lors de l'inscription. Sert en cas de changement de pseudo
	private String email;
	private String emailCanonical;
	private boolean enabled; // sert pour l'activation par email.
	private String salt; // le salt est une chaine de caractere aléatoire, différente pour chaque utilisateur qui est accolé au mot de passe lors du cryptage. Cela permet une plus grande résistance aux attaques par force brute par dictionnaire.
	private String password; // on ne stocke jamais le mot de passe en clair. On le crypte.
	private Timestamp lastLogin;
	private boolean locked; //est-ce qu'un utilisateur est banni ?
	private boolean expired; //sert pour les comptes avec abonnement. Ajoutés ici car on ne sait jamais.
	private Timestamp expiresAt;
	private String confirmationToken; // le token est une chaine de caractere unique ayant vocation a identifier l'utilisateur, notamment au moment de la validation de l'email.
	private String passwordRequestedAt; // A quelle a été demandé le nouveau mot de passe ?
	private int roles; // Champ de permission : 100-> administrateur, 50->Moderateur, 10->User standard, 0->Banni, 
	private boolean credentialsExpired;
	private Timestamp credentialsExpireAt;
	@OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
	private UserDetails userDetails;
	
    public User() {
        super();
    }
	
    /** Fonction hashant un mot de passe avec un salt
     * @param salt : une chaine de caractere unique de 10 caracteres.
     * @param password : la chaine a encoder
     * **/
    public String hash(String salt, String password) throws NoSuchAlgorithmException, UnsupportedEncodingException {
    	    MessageDigest crypt = MessageDigest.getInstance("SHA-1");
    	    crypt.reset();
    	    crypt.update((salt+password).getBytes("UTF-8"));
    	    return new BigInteger(1, crypt.digest()).toString(16);
    }
	
	
	@Override
	public String toString() {
		return "User{id="+id+", username=" + username+"}";
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getUsernameCanonical() {
		return usernameCanonical;
	}

	public void setUsernameCanonical(String usernameCanonical) {
		this.usernameCanonical = usernameCanonical;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getEmailCanonical() {
		return emailCanonical;
	}

	public void setEmailCanonical(String emailCanonical) {
		this.emailCanonical = emailCanonical;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public String getSalt() {
		return salt;
	}

	public void setSalt(String salt) {
		this.salt = salt;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Timestamp getLastLogin() {
		return lastLogin;
	}

	public void setLastLogin(Timestamp lastLogin) {
		this.lastLogin = lastLogin;
	}

	public boolean isLocked() {
		return locked;
	}

	public void setLocked(boolean locked) {
		this.locked = locked;
	}

	public boolean isExpired() {
		return expired;
	}

	public void setExpired(boolean expired) {
		this.expired = expired;
	}

	public Timestamp getExpiresAt() {
		return expiresAt;
	}

	public void setExpiresAt(Timestamp expiresAt) {
		this.expiresAt = expiresAt;
	}

	public String getConfirmationToken() {
		return confirmationToken;
	}

	public void setConfirmationToken(String confirmationToken) {
		this.confirmationToken = confirmationToken;
	}

	public String getPasswordRequestedAt() {
		return passwordRequestedAt;
	}

	public void setPasswordRequestedAt(String passwordRequestedAt) {
		this.passwordRequestedAt = passwordRequestedAt;
	}

	public int getRoles() {
		return roles;
	}

	public void setRoles(int roles) {
		this.roles = roles;
	}

	public boolean isCredentialsExpired() {
		return credentialsExpired;
	}

	public void setCredentialsExpired(boolean credentialsExpired) {
		this.credentialsExpired = credentialsExpired;
	}

	public Timestamp getCredentialsExpireAt() {
		return credentialsExpireAt;
	}

	public void setCredentialsExpireAt(Timestamp credentialsExpireAt) {
		this.credentialsExpireAt = credentialsExpireAt;
	}

	public UserDetails getUserDetails() {
		return userDetails;
	}

	public void setUserDetails(UserDetails userDetails) {
		this.userDetails = userDetails;
	}

	
    
}