package com.salescode.entity;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Entity
public class Notification {

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	private String notificationType;
	@OneToOne
	private User userTo; // l'id du user à qui c'est destiné.
	/** @note : je n'ai pas mis from et to car ce sont des mots réservés SQL. **/
	private String title;
	private String message;
	private Timestamp createdOn;
	private boolean isRead; // est-ce que la notif à été lue.
	
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNotificationType() {
		return notificationType;
	}
	public void setNotificationType(String notificationType) {
		this.notificationType = notificationType;
	}

	public User getUserTo() {
		return userTo;
	}
	public void setUserTo(User userTo) {
		this.userTo = userTo;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}


	public Timestamp getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Timestamp createdOn) {
		this.createdOn = createdOn;
	}
	public boolean isRead() {
		return isRead;
	}
	public void setRead(boolean read) {
		this.isRead = read;
	}

	
	/**
	 * Pour créer une nouvelle notification : on la construit puis on la persiste.
	 * Pour la marquer lue : setRead(true);
	 */
}