package com.salescode.entity;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(
	    uniqueConstraints={
    		@UniqueConstraint(columnNames={"title","creator_id"}),//un utilisateur ne peut avoir qu'un depot du même nom
	        @UniqueConstraint(columnNames={"path"})
	    }
	)
public class Repository {

	//un repository orienté commerce : avec des options de mise en forme, un titre une image, qui n'ont pas besoin d'exister dans un repository orienté git
	
	@Id
	@GeneratedValue( strategy = GenerationType.IDENTITY)
	private int id;
	private String title;
	private Timestamp createdOn;
	private Timestamp modifiedOn;
	private String path;
	@OneToOne
	private User creator; // l'id du createur : one Repository has one creator.
	private String image; // l'url d'une ressource url d'image.
	private String description; //plus pratique de stocker dans la BDD une description que d'aller lire les éventuels Readme.md de chaque depot.
	private float price;
	private int sales; // un champ pour comptabiliser les ventes de ce depot.
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public Timestamp getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Timestamp createdOn) {
		this.createdOn = createdOn;
	}
	public Timestamp getModifiedOn() {
		return modifiedOn;
	}
	public void setModifiedOn(Timestamp modifiedOn) {
		this.modifiedOn = modifiedOn;
	}
	public String getPath() {
		return path;
	}
	public void setPath(String path) {
		this.path = path;
	}
	public User getCreator() {
		return creator;
	}
	public void setCreator(User creator) {
		this.creator = creator;
	}
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public float getPrice() {
		return price;
	}
	public void setPrice(float price) {
		this.price = price;
	}
	
	
	public int getSales() {
		return sales;
	}
	public void setSales(int sales) {
		this.sales=sales;
	}
	public void addSales(){
		this.sales++;
	}
	public String toString(){
		return this.getTitle()+" by "+this.getCreator().getUsername()+" ("+this.getPath()+")";
	}
	
}