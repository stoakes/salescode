package com.salescode.entity;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Column;


/** Entity d'un commentaire (retours client) sur un depot. **/

@Entity
public class Commentary implements Serializable{
	
	@Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	@OneToOne
	private Repository repository; //l'id de l'issue correspondant à ce
	private String message;
	private String createdOn;
	private String modifiedOn;
	@OneToOne
	private User creator;
	private String action; // un issue peut etre fermé ou ouvert plusieurs fois. On garde donc un champ status pouvant prendre 3 valeurs : open, close, null. Un message de texte a un status null.
	/** One commentary has one issue **/
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Repository GetRepository() {
		return repository;
	}
	public void setRepository(Repository repository) {
		this.repository = repository;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(String createdOn) {
		this.createdOn = createdOn;
	}
	public String getModifiedOn() {
		return modifiedOn;
	}
	public void setModifiedOn(String modifiedOn) {
		this.modifiedOn = modifiedOn;
	}
	public User getCreator() {
		return creator;
	}
	public void setCreator(User creator) {
		this.creator = creator;
	}
	public String getAction() {
		return action;
	}
	public void setAction(String action) {
		this.action = action;
	}
	
	
	
}
