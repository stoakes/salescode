package com.salescode.entity;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

@Entity
public class Label {
	
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
    @OneToOne
	private Repository repository;
	private String title;
	@OneToMany
	private Collection<Issue> isssues = new ArrayList<Issue>();
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Repository getRepository() {
		return repository;
	}
	public void setRepository(Repository repository) {
		this.repository = repository;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public Collection<Issue> getIsssues() {
		return isssues;
	}
	public void setIsssues(Collection<Issue> isssues) {
		this.isssues = isssues;
	}
	
}