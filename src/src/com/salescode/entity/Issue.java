package com.salescode.entity;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import com.salescode.entity.IssueCommentary;

@Entity
public class Issue {
	
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	private String title;
	private String message;
	@OneToOne
	private User creator;
	private Timestamp createdOn;
	private Timestamp modifiedOn;
	private boolean closed; // est ce qu'un issue est marqué comme clot.
	private Timestamp closedOn;
	@OneToOne
	private User closeUser;
	@OneToOne
	private Repository repository;//le depot auquel est attachée l'issue.
	@OneToMany(fetch = FetchType.EAGER, mappedBy = "id", cascade = CascadeType.ALL)
	private Collection<IssueCommentary> commentaries = new ArrayList<IssueCommentary>();
	
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public User getCreator() {
		return creator;
	}
	public void setCreator(User creator) {
		this.creator = creator;
	}
	public Timestamp getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Timestamp createdOn) {
		this.createdOn = createdOn;
	}
	public Timestamp getModifiedOn() {
		return modifiedOn;
	}
	public void setModifiedOn(Timestamp modifiedOn) {
		this.modifiedOn = modifiedOn;
	}
	public boolean isClosed() {
		return closed;
	}
	public void setClosed(boolean closed) {
		this.closed = closed;
	}
	public Timestamp getClosedOn() {
		return closedOn;
	}
	public void setClosedOn(Timestamp closedOn) {
		this.closedOn = closedOn;
	}
	
	public User getCloseUser() {
		return closeUser;
	}
	public void setCloseUser(User closeUser) {
		this.closeUser = closeUser;
	}
	public Collection<IssueCommentary> getCommentaries() {
		return commentaries;
	}
	public void setCommentaries(Collection<IssueCommentary> commentaries) {
		this.commentaries = commentaries;
	}
	public Repository getRepository() {
		return repository;
	}
	public void setRepository(Repository repository) {
		this.repository = repository;
	}

	
	

}