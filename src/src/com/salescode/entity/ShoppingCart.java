package com.salescode.entity;

import java.util.Hashtable;
import java.util.Map.Entry;

public class ShoppingCart {
	private Hashtable<Integer, Purchase> listPurchase;
	
	public ShoppingCart() {
		listPurchase=new Hashtable<Integer, Purchase>();

	}
	
	
	public Hashtable<Integer, Purchase> getListPurchase() {
		return listPurchase;
	}
	
	public void setListPurchase(Hashtable<Integer, Purchase> listPurchase) {
		this.listPurchase = listPurchase;
	}


	public void addRepository(Purchase purchase){
		
		if(!listPurchase.containsKey(purchase.getRepository().getId())){
		this.listPurchase.put(purchase.getRepository().getId(),purchase);
		} else {
		int i=purchase.getQuantity();
		purchase.setQuantity(i+1);
		this.listPurchase.put(purchase.getRepository().getId(), purchase);
		}
	}

	public void removeRepository(Purchase purchase){
		listPurchase.remove(purchase.getRepository().getId());
	}
	
	public float getPrice(){
		float sum=0;
		for (Entry<Integer, Purchase> e : listPurchase.entrySet()){
		    sum=sum+e.getValue().getRepository().getPrice();
		}
		return sum;
	}
	
	
}
