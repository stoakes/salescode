package com.salescode.entity;

public class Purchase {
	private Repository repository;
	private int quantity;

	public Purchase(Repository repo){
		repository=repo;
		quantity=1;
	}

	public Repository getRepository() {
		return repository;
	}

	public void setRepository(Repository repository) {
		this.repository = repository;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	
}
