package com.salescode.entity;

import java.sql.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;


/**
 * Classe implementant la partie visible d'un utilisateur : son nom, son prenom, sa date de naissance, eventuellement une photo et une description. 
 * Permet de séparer la logique applicative profonde d'une partie plus libre, plus variable.
 * @author Antoine
 *
 *
 *@note Heritage car meme dans la partie affichage on peut avoir besoin des infos de bases.
 */
@Entity
public class UserDetails {
	
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	private String name;
	private String surname;
	private String birthdate;
	private String avatar;
	private String description;
	private String city;
	private String country;

	
	public String getName() {
		return name;
	}



	public void setName(String name) {
		this.name = name;
	}



	public String getSurname() {
		return surname;
	}



	public void setSurname(String surname) {
		this.surname = surname;
	}



	public String getBirthdate() {
		return birthdate;
	}



	public void setBirthdate(String birthdate) {
		this.birthdate = birthdate;
	}



	public String getAvatar() {
		return avatar;
	}



	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}



	public String getDescription() {
		return description;
	}



	public void setDescription(String description) {
		this.description = description;
	}



	public String getCity() {
		return city;
	}



	public void setCity(String city) {
		this.city = city;
	}



	public String getCountry() {
		return country;
	}



	public void setCountry(String country) {
		this.country = country;
	}


	@Override
	public String toString() {
		return "UserDetails [name=" + name + ", surname=" + surname + ", birthdate=" + birthdate + ", avatar=" + avatar
				+ ", description=" + description + ", city=" + city + ", country=" + country + "]";
	}
	
	
	
}