package com.salescode.test;


import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;

import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.lib.Constants;
import org.eclipse.jgit.lib.ObjectId;
import org.eclipse.jgit.lib.ObjectLoader;
import org.eclipse.jgit.lib.Repository;
import org.eclipse.jgit.revwalk.RevCommit;
import org.eclipse.jgit.revwalk.RevTree;
import org.eclipse.jgit.revwalk.RevWalk;
import org.eclipse.jgit.treewalk.TreeWalk;
import org.eclipse.jgit.treewalk.filter.PathFilter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.salescode.entity.User;
import com.salescode.form.ConnexionForm;

@WebServlet("/JGitTest")
public class JGitTest extends HttpServlet {

	/** totalement experimental, a pour but de lancer des tests depuis le client web sur le serveur. **/
	
	public JGitTest() {
        super();
    }
	 
	
	public void doGet( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException {

		   File gitWorkDir = new File("C:/temp/gittest/");
		   Git git = Git.open(gitWorkDir);
		   Repository repo = git.getRepository();

		   ObjectId lastCommitId = repo.resolve(Constants.HEAD);

		   RevWalk revWalk = new RevWalk(repo);
		   RevCommit commit = revWalk.parseCommit(lastCommitId);

		   RevTree tree = commit.getTree();

		   TreeWalk treeWalk = new TreeWalk(repo);
		   treeWalk.addTree(tree);
		   treeWalk.setRecursive(true);
		   treeWalk.setFilter(PathFilter.create("file1.txt"));
		   if (!treeWalk.next()) 
		   {
		     System.out.println("Nothing found!");
		     return;
		   }

		   ObjectId objectId = treeWalk.getObjectId(0);
		   ObjectLoader loader = repo.open(objectId);

		   ByteArrayOutputStream out = new ByteArrayOutputStream();
		   loader.copyTo(out);
		   System.out.println("file1.txt:\n" + out.toString());
	   }


    public void doPost( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException {
    	doGet(request,response);
    }
}




