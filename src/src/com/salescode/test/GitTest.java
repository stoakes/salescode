package com.salescode.test;

import java.io.File;
import java.io.IOException;

import org.eclipse.jgit.lib.Ref;
import org.eclipse.jgit.revwalk.RevCommit;
import org.eclipse.jgit.revwalk.RevTree;
import org.eclipse.jgit.revwalk.RevWalk;
import org.eclipse.jgit.storage.file.FileRepositoryBuilder;
import org.eclipse.jgit.treewalk.TreeWalk;


public class GitTest {
 public static void Main(String[] argv) throws IOException{
		FileRepositoryBuilder builder = new FileRepositoryBuilder();

     org.eclipse.jgit.lib.Repository repository = builder
             .setGitDir(new File("C:/temp/stoakes/Changelog/")).readEnvironment()
             .findGitDir().build();
     Ref head = repository.getRef("HEAD");
     System.out.println("fatigue");

     // a RevWalk allows to walk over commits based on some filtering that is
     // defined
     RevWalk walk = new RevWalk(repository);

     RevCommit commit = walk.parseCommit(head.getObjectId());
     RevTree tree = commit.getTree();
     System.out.println("Having tree: " + tree);

     // now use a TreeWalk to iterate over all files in the Tree recursively
     // you can set Filters to narrow down the results if needed
     TreeWalk treeWalk = new TreeWalk(repository);
     treeWalk.addTree(tree);
     treeWalk.setRecursive(false);
     while (treeWalk.next()) {
         if (treeWalk.isSubtree()) {
             System.out.println("dir: " + treeWalk.getPathString());
             treeWalk.enterSubtree();
         } else {
             System.out.println("file: " + treeWalk.getPathString());
         }
     }

     // Close repo
     repository.close();
 }
}
