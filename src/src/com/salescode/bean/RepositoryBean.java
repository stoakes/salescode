package com.salescode.bean;

import java.util.List;

import javax.ejb.Singleton;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import com.salescode.entity.Repository;
import com.salescode.entity.User;

@Singleton
public class RepositoryBean {
 	 
	@PersistenceContext(unitName = "SalescodePU")
	private EntityManager em;
	     
	    public void Repository() {   }
	 
	   //@Override
	    public void save(Repository repository) {
	    	 try {
	 	        em.persist(repository);//ajoute dans la liste des objets managés
	 	        em.flush(); //persiste en BDD.
	 		} catch (Exception e) {
	 			e.printStackTrace();
	 		}
	    }
	    
	    public void merge(Repository repository) {//Update un repository d�j� existant dans la BDD
	    	 try {
	 	        em.merge(repository);//ajoute dans la liste des objets managés
	 	        em.flush(); //persiste en BDD.
	 		} catch (Exception e) {
	 			e.printStackTrace();
	 		}
	    }
	 
//	    @Override
	    public Repository find(Repository eRepository) {
	        Repository p = em.find(Repository.class, eRepository.getId());
	        return p;
	    }
	    
	    public Repository findById(int id) {
	        Repository p = em.find(Repository.class, id);
	        return p;
	    }
	 
	  //  @Override
	    public List<Repository> retrieveAllRepositories() {
	         
	        String q = "SELECT p from " + Repository.class.getName() + " p";
	        Query query = em.createQuery(q);
	        List<Repository> eRepositoryList = query.getResultList();
	        return eRepositoryList;
	    }
	    
		  //  recupere tous les repositories dont le prix est > 0 (donc ceux que l'on peut acheter)
	    public List<Repository> retrieveAllECommerceRepositories() {
	         
	        String q = "SELECT p from " + Repository.class.getName() + " p where price > 0";
	        Query query = em.createQuery(q);
	        List<Repository> eRepositoryList = query.getResultList();
	        return eRepositoryList;
	    }
	    
	    public List<Repository> findByCreator(User user){
	    	//preparation de la requete pour eviter les injections SQL.
	    	Query q = em.createQuery("from " + Repository.class.getName() + " as r where r.creator = :user ",Repository.class);
	    	q.setParameter("user",  user);
	    	List<Repository> result = q.getResultList();
	    	return result;
	    }
	    
	    
	    //retourne l'unique repo de l'utilisateur ayant ce titre.
	    public Repository findByTitleCreator(User creator,String title) throws NoResultException{
	    	//preparation de la requete pour eviter les injections SQL.
	    	Repository result = null;
	    	try{ //obligé de mettre un catcheur au cas ou c'est vide ! youpi !
		    	Query q = em.createQuery("from Repository as r where r.title = :title and r.creator = :creator ",Repository.class);
	    		result = (Repository) q.setParameter("title", title).setParameter("creator", creator).getSingleResult();
	    	}
	    	catch(NullPointerException | NoResultException e){ result = null; }
	    	return result;
	    }
	    
	    
	    //retourne tous les depots ayant le titre passé en param
	    public List<Repository> findByTitle(String title) throws NoResultException{
	    	//preparation de la requete pour eviter les injections SQL.
	    	Query q = em.createQuery("from Repository as r where r.title = :title ",Repository.class);
	    	q.setParameter("title", title);
			List<Repository> result = q.getResultList();
	    	return result;
	    }
	}