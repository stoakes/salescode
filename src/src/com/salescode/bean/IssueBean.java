package com.salescode.bean;

import java.util.List;

import javax.ejb.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import com.salescode.entity.Issue;
import com.salescode.entity.IssueCommentary;
import com.salescode.entity.Repository;

//permet de gérer des issues et des IssuesCommentary.
@Singleton
public class IssueBean {


	@PersistenceContext(unitName = "SalescodePU")
	private EntityManager em;

	public void issueBean(){}

	//@Override
	public void save(Issue issue) {
		try {
			em.persist(issue);//ajoute dans la liste des objets managés
			em.flush(); //persiste en BDD.
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void saveCommentary(IssueCommentary iC) {
		try {
			em.persist(iC);//ajoute dans la liste des objets managés
			em.flush(); //persiste en BDD.
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	//    @Override
	public Issue find(Issue issue) {
		Issue i = em.find(Issue.class, issue.getId());
		return i;
	}

	public Issue findById(int issueId) {
		Issue i = em.find(Issue.class, issueId);
		return i;
	}

	public List<Issue> findByRepository(Repository repo) {
		Query q = em.createQuery("from " + Issue.class.getName() + " as i where i.repository = :repository ",Issue.class);
		q.setParameter("repository",  repo);
		List<Issue> result = q.getResultList();
		return result;
	}

	public void mergeIssue(Issue i) {
		try {
			em.merge(i);//ajoute dans la liste des objets managés
			em.flush(); //persiste en BDD.
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
