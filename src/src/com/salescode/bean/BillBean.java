package com.salescode.bean;

import java.util.List;

import javax.ejb.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import com.salescode.entity.Bill;
import com.salescode.entity.Repository;
import com.salescode.entity.User;


	@Singleton
	public class BillBean {


		@PersistenceContext(unitName = "SalescodePU")
		private EntityManager em;
		
		public void billBean(){}
		
		//@Override
	    public void save(Bill bill) {
	        try {
		        em.persist(bill);//ajoute dans la liste des objets managés
		        em.flush(); //persiste en BDD.
			} catch (Exception e) {
				e.printStackTrace();
			}
	    }
	    
	    public void merge(Bill bill) {//Update une facture d�j� existant dans la BDD
	    	 try {
	 	        em.merge(bill);//ajoute dans la liste des objets managés
	 	        em.flush(); //persiste en BDD.
	 		} catch (Exception e) {
	 			e.printStackTrace();
	 		}
	    }
	 
	    public List<Bill> findByCreator(User user){
	    	//preparation de la requete pour eviter les injections SQL.
	    	Query q = em.createQuery("from " + Bill.class.getName() + " as r where r.userTo = :userTo ",Bill.class);
	    	q.setParameter("userTo",  user);
	    	List<Bill> result = q.getResultList();
	    	return result;
	    }
	    
	    public Bill findById(int id) {
	    	Bill b = em.find(Bill.class, id);
	    	return b;
	    }
	    
	   

}
