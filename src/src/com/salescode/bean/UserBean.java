package com.salescode.bean;

import java.util.List;

import javax.ejb.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import com.salescode.entity.Repository;
import com.salescode.entity.User;


@Singleton
public class UserBean {


	@PersistenceContext(unitName = "SalescodePU")
	private EntityManager em;
	
	public void userBean(){}
	
	//@Override
    public void save(User user) {
        try {
	        em.persist(user);//ajoute dans la liste des objets managés
	        em.flush(); //persiste en BDD.
		} catch (Exception e) {
			e.printStackTrace();
		}
    }
    

//  @Override
    public List<User> retrieveAllUsers() {
         
        String q = "from " + User.class.getName() + " ";
        Query query = em.createQuery(q);
        List<User> userList = query.getResultList();
        return userList;
    }

  //@Override
    public void merge(User user) {//Update un repository d�j� existant dans la BDD
   	 try {
	        em.merge(user);//ajoute dans la liste des objets managés
	        em.flush(); //persiste en BDD.
		} catch (Exception e) {
			e.printStackTrace();
		}
   }
 
    //    @Override
    public User find(User user) {
        User u = em.find(User.class, user.getId());
        return u;
    }

    public User findById(int id) {
        User u = em.find(User.class,id);
        return u;
    }
    
    /** Find by Email : permet de recupérer un utilisateur par son email. On sait qu'email est Unique sur la table user. **/
    public User findByEmail(String email){
    	//preparation de la requete pour eviter les injections SQL.
    	User result = null;
    	try{
    		Query q = em.createQuery("from User as u where u.email = :email ",User.class);
    		 result = (User) q.setParameter("email", email).getSingleResult();
    	}catch(NullPointerException | NoResultException e){ result = null; }

    	return result;
    }
    
    /** Find by Username : permet de recupérer un utilisateur par son username. On sait que username est Unique sur la table user. **/
    public User findByUsername(String username){
    	//preparation de la requete pour eviter les injections SQL.
    	User result = null;
    	try{
    		Query q = em.createQuery("from User as u where u.username = :username ",User.class);
    		result = (User) q.setParameter("username", username).getSingleResult();
    		
    	}catch(NullPointerException | NoResultException e){ result = null; }

    	return result;
    }
    
    public List<User> retrieveAllUser() {
        
        String q = "SELECT p from " + User.class.getName() + " p";
        Query query = em.createQuery(q);
        List<User> userList = query.getResultList();
        return userList;
    }
}
