package com.salescode.bean;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.List;

import javax.ejb.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;

import com.salescode.entity.Issue;
import com.salescode.entity.Notification;
import com.salescode.entity.User;

@Singleton
public class NotificationBean {


	@PersistenceContext(unitName = "SalescodePU")
	private EntityManager em;
	
	public void notificationBean(){}
	
	//@Override
    public void save(Notification notif) {
        try {
	        em.persist(notif);//ajoute dans la liste des objets managés
	        em.flush(); //persiste en BDD.
		} catch (Exception e) {
			e.printStackTrace();
		}
    }
    
    /**
     * méthode pour créer directement une notification. A usage interne, ne pas y mettre des données envoyées par les utilisateurs (car pas de vérification de données).
     * @param notificationType
     * @param userTo
     * @param title
     * @param message
     * 
     * utilisation : notificationBean.newNotification(....);
     */
    public void newNotification(String notificationType, User userTo, String title, String message){
    	Notification n = new Notification();
    	n.setNotificationType(notificationType);
    	n.setUserTo(userTo);
    	n.setTitle(title);
    	n.setMessage(message);
    	n.setCreatedOn(new Timestamp(new java.util.Date().getTime()));
    	n.setRead(false);
    	em.persist(n);
    	em.flush();
    }
 
    //    @Override
    public Notification find(Notification notif) {
        Notification n = em.find(Notification.class, notif.getId());
        return n;
    }

    public Notification findById(int id) {
        Notification n = em.find(Notification.class,id);
        return n;
    }
    
    /** Find by UserTo : permet de recupérer les notifications de l'utilisateur userFrom. On sait que userFrom est Unique sur la table User. **/
    public List<Notification> findByUserTo(User userTo){

    	List<Notification> resultList = null;
    	try{
    		Query q = em.createQuery("from " + Notification.class.getName() + " n  where n.userTo = :userTo ",Notification.class);
    		q.setParameter("userTo", userTo);
    		resultList = (List<Notification>) q.getResultList();
    	}
    	catch(NullPointerException e){ resultList = null; }

    	return resultList;
    }
    
    public List<Notification> findByUserToUnRead(User userTo){

    	List<Notification> resultList = null;
    	try{
    		Query q = em.createQuery("from " + Notification.class.getName() + " n where n.userTo = :userTo and isRead=false",Notification.class);
    		q.setParameter("userTo", userTo);
    		resultList = (List<Notification>) q.getResultList();
    	}
    	catch(NullPointerException e){ resultList = null; }

    	return resultList;
    }
    
    /** countUnreadNotif : permet de compter le nombre de notifications non lues de listenotif. **/
    public int countUnreadNotif(User userTo){
    	//overKill, mais je ne peux pas passer 1h sur une requete. #failStackOverflow.
    	//TODO : improve performance
    	return findByUserToUnRead(userTo).size();
    	
    }
    
    /** updateNotif : met toutes les notifications de userTo a true (donc lues)*/
    public void readNotif(User userTo){
    	
    	Query query = em.createQuery("UPDATE " + Notification.class.getName() + " SET isRead = true where userTo = :userTo");
    	query.setParameter("userTo", userTo).executeUpdate();
			
    }
}
