package com.salescode.misc;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;

public class Paypal {
	private String apiPaypal;
	private String version ; // Version de l'API
	private String userAPI;
	private String pwdAPI;
	private String signature;
	private String method;
	private String cancelUrl;
	private String returnUrl;
	private String desc;
	private String hdrimg;

	private ArrayList<String> listParam=new ArrayList<String>();
	
	public Paypal() throws UnsupportedEncodingException{
		apiPaypal="https://api-3t.sandbox.paypal.com/nvp?";
		version= "124";
		userAPI="teamsalescode_api1.gmail.com";
		pwdAPI="A7V747UVPA7P9UT2";
		signature="AkqvibwMPIB0J2WQ.u7VSWi9kgfAAK03b6syL5I71J0yr0vVxMgP4pNh";
		method="SetExpressCheckout";
		cancelUrl="http://localhost:8080/salescode/ShoppingCart";
		returnUrl="http://localhost:8080/salescode/ShoppingCart";
		desc=URLEncoder.encode("Achat de dépots sur le site Salecode.fr","UTF-8");
		hdrimg=URLEncoder.encode("http://img11.hostingpics.net/pics/208803logo.png","UTF-8");
		listParam=null;
	}
	
	public URL constructUrl(float price){
		String urlPaypal=apiPaypal+"VERSION="+version+"&USER="+userAPI+"&PWD="+pwdAPI+"&SIGNATURE="+signature+"&METHOD="+method
				+"&CANCELURL="+cancelUrl+"&RETURNURL="+returnUrl+"&AMT="+price+"&CURRENCYCODE=EUR"+"&DESC="+desc+"&HDRIMG="+hdrimg;
		System.out.println(urlPaypal);
		URL url = null;
		try {
			url = new URL(urlPaypal);
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
		return url;
	}
	
	public void recoverParameter(URL url) throws IOException{
		BufferedReader br;
		br = new BufferedReader(new InputStreamReader(url.openStream()));
		String strTemp = "";
		String domain = "";
		ArrayList<String> al = new ArrayList<String>();
		while (null != (strTemp = br.readLine())) {
			
			domain=domain+strTemp;
		}
		 String[] strArray = domain.split("&");
		 for (String str : strArray) {
			 al.add(str.substring(str.indexOf("=") + 1));
			 System.out.println(str.substring(str.indexOf("=") + 1));
		 }
		this.listParam=al;
	}
	
	public String redirectPaiement(){
		String url = "";
		url = "https://www.sandbox.paypal.com/webscr&cmd=_express-checkout&useraction=commit&token="+(String)listParam.get(0);
		System.out.println(url);
		
		return url;
	}
	
	public String paiementSucess(String token, String payerid,float price){
		String url ="";
		url=apiPaypal+"VERSION="+version+"&USER="+userAPI+"&PWD="+pwdAPI+"&SIGNATURE="+signature+"&METHOD=DoExpressCheckoutPayment"+"&TOKEN="+token
				+"&AMT="+price+"&CURRENCYCODE=EUR"+"&PayerID="+payerid+"&PAYMENTACTION=sale";
		return url;		
				
	}

	public String getMethod() {
		return method;
	}

	public void setMethod(String method) {
		this.method = method;
	}
	
}
