package com.salescode.form;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.Map;
import javax.ejb.EJB;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.salescode.bean.RepositoryBean;
import com.salescode.entity.Repository;
import com.salescode.entity.User;
import com.salescode.misc.HTMLEntities;

public final class RepositoryForm {
	
    public static final String TITLE_FIELD = "title";
    public static final String DESCRIPTION_FIELD = "description";
    public static final String PRICE_FIELD = "price";
    public static final String IMG_FIELD = "img";

    private Map<String, String> errors      = new HashMap<String, String>();
    @EJB
    private RepositoryBean repositoryBean = new RepositoryBean();

    public Map<String, String> getErrors() {
        return errors;
    }

    /** Procedure d'enregistrement d'un nouvel utilisateur.
     * Pour la m�thode preparant l'objet user qui sera persisté, @see createNewuser() 
     * @throws UnsupportedEncodingException 
     * @throws NoSuchAlgorithmException **/
    
    public Repository registerRepository( HttpServletRequest request ){
        String title = getFieldValue( request, TITLE_FIELD );
        String description = HTMLEntities.htmlentities(getFieldValue( request, DESCRIPTION_FIELD ));
        float price = getPriceValue( request, PRICE_FIELD );
        String img=getFieldValue(request,IMG_FIELD);
        
        HttpSession session = request.getSession();


        //verification sur les parametres
        try { isCorrectFormatTitle( title, session ); } 
        catch ( Exception e ) { setErrors( TITLE_FIELD, e.getMessage() ); }

        try { isCorrectFormatDescription( description ); } 
        catch ( Exception e ) {setErrors( DESCRIPTION_FIELD, e.getMessage() );}
        
        try { isCorrectFormatPrice( price );
        } catch ( Exception e ) { setErrors( PRICE_FIELD, e.getMessage() ); }
        
        
        Repository repository = null;

        if ( errors.isEmpty() ) {
			repository = createNewRepository((User) session.getAttribute("user"), title, description, price,img);
        }

        return repository;
    }
    
    /*
     * Methode preparant le nouvel utilisateur avant qu'il soit persisté.
     */

    private Repository createNewRepository(User creator, String title, String description, float price,String img){
    	Repository repository= new Repository();
    	repository.setTitle(title);
    	repository.setDescription(description);
    	repository.setPrice(price);
    	repository.setCreatedOn(new Timestamp(new java.util.Date().getTime()));
		repository.setCreator(creator);
		repository.setPath(creator.getUsername()+"/"+title);
		repository.setImage(img);
		repository.setSales(0);
    	return repository;
    }
    
    

    private void isCorrectFormatDescription( String description ) throws Exception {
    	if ( description != null && description.length() < 3 ) {
            throw new Exception( "La description doit contenir au moins 3 caractères." );
        }
    }

    private void isCorrectFormatTitle( String title , HttpSession session) throws Exception {

        if ( title != null && title.length() < 3 ) {
            throw new Exception( "Le titre doit contenir au moins 3 caractères." );
        }
        else{// verification de l'unicité du titre
        	Repository rTest = repositoryBean.findByTitleCreator((User) session.getAttribute("user"), title );
        	if(rTest != null){
        		throw new Exception("Ce titre existe déjà pour un de vos depots");
        	}
        }
    }
    private void isCorrectFormatPrice( float price ) throws Exception {
        if ( price == 0 && price < 0 ) {
            throw new Exception( "Le prix doit être supérieur à 0." );
        }
    }

    /*
     * Ajoute un message correspondant au champ spécifié à la map des erreurs.
     */
    private void setErrors( String champ, String message ) {
        errors.put( champ, message );
    }

    /*
     * Méthode utilitaire qui retourne null si un champ est vide, et son contenu
     * sinon.
     */
    private static String getFieldValue( HttpServletRequest request, String fieldName ) {
        String value = request.getParameter( fieldName );
        if ( value == null || value.trim().length() == 0 ) {
            return null;
        } else {
            return value.trim();
        }
    }
    private static float getPriceValue( HttpServletRequest request, String fieldName ) {
        float value = Float.parseFloat(request.getParameter( fieldName ));
        if ( value == 0  ) {
            return -1;
        } else {
            return value;
        }
    }
    
    
}