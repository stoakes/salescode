package com.salescode.form;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import com.salescode.bean.IssueBean;
import com.salescode.bean.RepositoryBean;
import com.salescode.bean.UserBean;
import com.salescode.entity.Issue;
import com.salescode.entity.IssueCommentary;
import com.salescode.entity.Repository;
import com.salescode.entity.User;
import com.salescode.misc.HTMLEntities;

public class IssuesForm {

	public static final String TITLE_FIELD = "title";
	public static final String MESSAGE_FIELD = "message";
	public static final String REPOSITORY_FIELD = "repository";
    public static final String ISSUE_FIELD = "issueMaster"; // champ d'indice pour l'issue. S'il est setté : on poste sur un issue existant déjà. Sinon, on en créé un nouveau.

    private Map<String, String> errors      = new HashMap<String, String>();
    
    private UserBean  userBean;
    private RepositoryBean  repositoryBean;
    private  IssueBean  issueBean ;
    
    
    public IssuesForm(UserBean u, IssueBean i, RepositoryBean r){
    	/* ne fonctionne pas */
    	/*         UserBean  userBean = (UserBean) context.getAttribute("userBean");    RepositoryBean  repositoryBean = (RepositoryBean) context.getAttribute("repositoryBean"); IssueBean  issueBean = (IssueBean) context.getAttribute("issueBean"); */
    	
    	this.userBean =u;
    	this.repositoryBean = r;
    	this.issueBean = i;
    	
    }
    
    /** Procedure d'enregistrement d'un nouvel issues **/
    public Issue registerNewIssue( HttpServletRequest request){
        HttpSession session = request.getSession();

        String title = getFieldValue( request, TITLE_FIELD );
        String message = getFieldValue( request, MESSAGE_FIELD );
        String repo = getFieldValue(request,"op");
        String creator = (String) request.getParameter("creator");
        
        Repository repository= null;
         

        try{
        	
        	repository = repositoryBean.findByTitleCreator(userBean.findByUsername(creator),repo );
        	System.out.println(repository);
        	if(repository == null){
        		setErrors( REPOSITORY_FIELD, "Ce repertoire n'existe pas" );
        	}
        }catch(Exception e){ setErrors( REPOSITORY_FIELD, "Ce repertoire n'existe pas" ); }

        //verification sur les parametres
        try { isCorrectFormatTitle( title ); }catch ( Exception e ) { setErrors( TITLE_FIELD, e.getMessage() ); }
        try { isCorrectFormatMessage(message); }catch ( Exception e ) {  setErrors( MESSAGE_FIELD, e.getMessage() ); }
        
        Issue issue = null;
        if ( errors.isEmpty() ) {
			issue = createNewIssue((User) session.getAttribute("user"), repository, title, message);
        } 

        return issue;
    }
    
        
    /** ajoute un nouveau commentaire à un issue **/
    public IssueCommentary registerNewIssueCommentary(HttpServletRequest request){
        HttpSession session = request.getSession();

    	String message= getFieldValue( request, MESSAGE_FIELD );
        String issueMasterId = getFieldValue( request, ISSUE_FIELD );
        int issueMaster = 0;
        if(issueMasterId != null){
        	issueMaster = Integer.parseInt(issueMasterId);
        }
        else{
        	issueMaster = 0;
        }
        
        //verification des parametres
        try { isCorrectFormatMessage( message); }catch(Exception e){  setErrors( MESSAGE_FIELD, e.getMessage()); }
        try { isCorrectFormatIssueMaster( issueMaster, issueBean ); } catch ( Exception e ) { setErrors( ISSUE_FIELD, e.getMessage() ); }
		
        //l'issueCommentary que l'on va persister
        IssueCommentary issueCommentary = null;      
        if ( errors.isEmpty() ) {
            issueCommentary = createNewIssueCommentary((User)session.getAttribute("user") , issueBean.findById(issueMaster), message);
        } 
        
        return issueCommentary;
        
    }
       
    public Map<String, String> getErreurs() {
        return this.errors;
    }
    
    /* créé un objet issue complet **/
    private Issue createNewIssue(User creator, Repository repository, String title, String message){
    	Issue i = new Issue();
    	i.setCreator(creator);
    	i.setTitle(title);
    	i.setMessage(HTMLEntities.htmlentities(message));
    	i.setClosed(false);
    	i.setCreatedOn( new Timestamp(new java.util.Date().getTime()) );
    	i.setRepository(repository);
    	return i;
    }

	private IssueCommentary createNewIssueCommentary(User creator, Issue issueMaster, String message){
    	IssueCommentary i = new IssueCommentary();
    	i.setCreator(creator);
    	i.setMessage(HTMLEntities.htmlentities(message) );
    	i.setCreatedOn( new Timestamp(new java.util.Date().getTime()) );
    	//i.setIssue(issueMaster);
		return i;
    }
    

    private void isCorrectFormatIssueMaster(int issueMaster, IssueBean issueBean) throws Exception {
		//on select l'issue qui aurait cet id. Si le résultat est nul (ie n'existe pas), on lance une exception.
		Issue i = issueBean.findById(issueMaster);
		if(i == null){
			throw new Exception("Cet issue n'existe pas.");
		}
	}
    
    /* Verifie qu'un repository existe selon cet id */
    private void isCorrectFormatRepository(int idrepository, RepositoryBean repositoryBean) throws Exception {
		//on select l'issue qui aurait cet id. Si le résultat est nul (ie n'existe pas), on lance une exception.
		Repository r = repositoryBean.findById(idrepository);
		if(r == null){
			throw new Exception("Ce repository n'existe pas.");
		}
	}

    /* verifie le format d'un message */
	private void isCorrectFormatMessage(String message) throws Exception {
		if ( message != null && message.length() < 5 ) {
            throw new Exception( "Le message doit contenir au moins 5 caractères." );
		}
	}

	/* verifie que le format du titre convient. */
    private void isCorrectFormatTitle(String title) throws Exception {
		if ( title != null && title.length() < 5 ) {
            throw new Exception( "Le titre doit contenir au moins 5 caractères." );
		}
	}

    /* Ajoute un message correspondant au champ spécifié à la map des erreurs. */
    private void setErrors( String champ, String message ) {
        this.errors.put( champ, message );
    }
    

    /* Méthode utilitaire qui retourne null si un champ est vide, et son contenu sinon. */
    private static String getFieldValue( HttpServletRequest request, String fieldName ) {
        String value = request.getParameter( fieldName );
        if ( value == null || value.trim().length() == 0 ) {
            return null;
        } else {
            return value.trim();
        }
    }
    
}
