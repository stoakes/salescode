package com.salescode.form;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.salescode.bean.UserBean;
import com.salescode.entity.User;

public final class ConnexionForm {
	
	/** Les 2 champs du formulaire : public pour etre visible de l'exterieur pour y faire **reference dynamiquement** **/
    private static final String EMAIL_FIELD  = "email";
    private static final String PASSWORD_FIELD   = "motdepasse";
    
    private boolean isConnected;//le resultat de l'authentification.
    private Map<String, String> errors = new HashMap<String, String>();
    
    public User connexion( HttpServletRequest request ) {
        /* Récupération des champs du formulaire */
        String email = this.getFieldValue( request, EMAIL_FIELD );
        String password = this.getFieldValue( request, PASSWORD_FIELD );

        /* Validation du champ email & mot de passe. */
        try {
            isCorrectEmailFormat( email );
        } catch ( Exception e ) {
            this.setError( EMAIL_FIELD, e.getMessage() );
        }
        try {
            isCorrectPasswordFormat(password);
        } catch ( Exception e ) {
            this.setError( PASSWORD_FIELD, e.getMessage() );
        }        
        
        /*  S'il n'y a pas d'erreurs, on créé un user avec l'email et le mot de passe pret */
        User user = new User();
        user.setEmail(email);
        user.setPassword(password);
        return user;

    }

    
    /**
     * Valide l'adresse email saisie.
     */
    private void isCorrectEmailFormat( String email ) throws Exception {
        if ( email != null && !email.matches( "([^.@]+)(\\.[^.@]+)*@([^.@]+\\.)+([^.@]+)" ) ) {
            throw new Exception( "Adresse mail invalide." );
        }
    }

    /** Valide le mot de passe saisi.  */
    private void isCorrectPasswordFormat( String password ) throws Exception {
        if ( password != null ) {
            if ( password.length() < 3 ) {
                throw new Exception( "Le mot de passe doit contenir au moins 3 caractères." );
            }
        } else {
            throw new Exception( "Mot de passe vide." );
        }
    }

    
    /**  Ajoute un message correspondant au champ spécifié à la map des erreurs.  **/
    private void setError( String champ, String message ) {
        errors.put( champ, message );
    }

    /*
     * Méthode utilitaire qui retourne null si un champ est vide, et son contenu
     * sinon.
     */
    private static String getFieldValue( HttpServletRequest request, String nomChamp ) {
        String valeur = request.getParameter( nomChamp );
        if ( valeur == null || valeur.trim().length() == 0 ) {
            return null;
        } else {
            return valeur;
        }
    }
    
    
    public boolean getIsConnected() {
        return isConnected;
    }

    public Map<String, String> getErrors() {
        return errors;
    }
}