package com.salescode.form;

import java.util.HashMap;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;

import com.salescode.bean.UserBean;
import com.salescode.entity.User;

public final class ProfilForm {
	public static final String NAME_FIELD = "name";
	public static final String SURNAME_FIELD = "surname";
	public static final String BIRTHDATE_FIELD = "birthdate";
	public static final String AVATAR_FIELD = "avatar";
	public static final String DESCRIPTION_FIELD = "description";
	public static final String CITY_FIELD = "city";
	public static final String COUNTRY_FIELD = "country";
	public static final String EMAIL_FIELD = "email";
	private Map<String, String> errors = new HashMap<String, String>();
    private UserBean userBean;
    
    public ProfilForm(UserBean u){
    	this.userBean =u;
    }
	
	/** Procedure d'enregistrement d'un nouvel utilisateur.
     * Pour la méthode preparant l'objet user qui sera persisté, @see createNewuser() 
	 * @throws Exception **/
    public User registerNewEditUser( HttpServletRequest request, User user) throws Exception {
    	String name = getFieldValue( request, NAME_FIELD );
    	String surname = getFieldValue( request, SURNAME_FIELD );
    	/*Date birthdate = getDateValue( request, BIRTHDATE_FIELD );*/
    	String birthdate = getFieldValue( request, BIRTHDATE_FIELD );
    	String avatar = getFieldValue( request, AVATAR_FIELD );
    	String description = getFieldValue( request, DESCRIPTION_FIELD );
    	String city = getFieldValue( request, CITY_FIELD );
    	String country = getFieldValue( request, COUNTRY_FIELD );
    	String email = getFieldValue( request, EMAIL_FIELD );
        

    	//verification sur les parametres
    	try { isCorrectFormatName( name );
        } catch ( Exception e ) { setErrors( NAME_FIELD, e.getMessage() ); }
    	try { isCorrectFormatName( surname );
        } catch ( Exception e ) { setErrors( SURNAME_FIELD, e.getMessage() ); }
    	try { isCorrectFormatDate( birthdate );
        } catch ( Exception e ) { setErrors( BIRTHDATE_FIELD, e.getMessage() ); }
    	try { isCorrectFormatName( avatar );
        } catch ( Exception e ) { setErrors( AVATAR_FIELD, e.getMessage() ); }
    	try { isCorrectFormatName( description );
        } catch ( Exception e ) { setErrors( DESCRIPTION_FIELD, e.getMessage() ); }
    	try { isCorrectFormatName( city );
        } catch ( Exception e ) { setErrors( CITY_FIELD, e.getMessage() ); }
    	try { isCorrectFormatName( country );
        } catch ( Exception e ) { setErrors( COUNTRY_FIELD, e.getMessage() ); }
    	try { isCorrectFormatEmail( email ); } 
        catch ( Exception e ) { setErrors( EMAIL_FIELD, e.getMessage() ); }
        
        //User user = null;
        if ( errors.isEmpty() ) {
			//modification de l'objet en session
        	
        	user.getUserDetails().setAvatar(name);
        	user.getUserDetails().setSurname(surname);
        	user.getUserDetails().setBirthdate(birthdate);
        	user.getUserDetails().setAvatar(avatar);
        	user.getUserDetails().setDescription(description);
        	user.getUserDetails().setCity(city);
        	user.getUserDetails().setCountry(country);
        	
        }
        return user;
    }
    
    /*private Date getDateValue(HttpServletRequest request, String birthdateField) throws Exception {
    	Date date = null;
    	String birthdate = request.getParameter( birthdateField );
    	System.out.println("debug : birthdate" + birthdate);
    	
    	SimpleDateFormat formatter = new SimpleDateFormat("dd/mm/yyyy");
        if ( birthdate != null ) {
            if ( !birthdate.matches( "[0-9]{2}/[0-9]{2}/[0-9]{4}" ) ) {
                throw new Exception( "Birthdate invalide." );
            } else {
            	date = (java.sql.Date) formatter.parse(birthdate);
            }
        } else {
            throw new Exception( "Birthdate non renseignee." );
        }
		return date;
	}*/
    
  

	private void isCorrectFormatDate(String birthdate) throws Exception {
		if ( birthdate != null ) {
            if ( !birthdate.matches( "[0-9]{2}/[0-9]{2}/[0-9]{4}" ) ) {
                throw new Exception( "Birthdate invalide." );
            } 
		} else {
            throw new Exception( "Birthdate non renseignee." );
        }
	}

	/*
     * Methode utilitaire qui retourne null si un champ est vide, et son contenu
     * sinon.
     */
    private static String getFieldValue( HttpServletRequest request, String fieldName ) {
        String value = request.getParameter( fieldName );
        if ( value == null || value.trim().length() == 0 ) {
            return null;
        } else {
            return value.trim();
        }
    }
    
    private void isCorrectFormatName( String name ) throws Exception {
        if ( name != null && name.length() < 3 ) {
            throw new Exception( "Le nom d'utilisateur doit contenir au moins 3 caracteres." );
        }
    }
	
    /*
     * Ajoute un message correspondant au champ spécifié à la map des erreurs.
     */
    private void setErrors( String champ, String message ) {
        errors.put( champ, message );
    }
    private void isCorrectFormatEmail( String email ) throws Exception {
        if ( email != null ) {
            if ( !email.matches( "([^.@]+)(\\.[^.@]+)*@([^.@]+\\.)+([^.@]+)" ) ) {
                throw new Exception( "Adresse mail invalide." );
            }
        } else {
            throw new Exception( "Adresse mail non renseignee." );
        }
    }

    
    
    public Map<String, String> getErrors() {
		return errors;
	}

	/*private void isCorrectPasssword( String password, String confirmation ) throws Exception {
        if ( password != null && confirmation != null ) {
            if ( !password.equals( confirmation ) ) {
                throw new Exception( "Les mots de passe entrés sont différents, merci de les saisir à nouveau." );
            } else if ( password.length() < 3 ) {
                throw new Exception( "Les mots de passe doivent contenir au moins 3 caractères." );
            }
        } else {
            throw new Exception( "Merci de saisir et confirmer votre mot de passe." );
        }
    }*/

}