package com.salescode.form;

import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.text.Normalizer;
import java.text.Normalizer.Form;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;

import com.salescode.bean.UserBean;
import com.salescode.entity.User;
import com.salescode.entity.UserDetails;

public final class InscriptionForm {
	
    public static final String EMAIL_FIELD = "email";
    public static final String PASSWORD_FIELD = "password";
    public static final String VERIF_FIELD = "confirmation";
    public static final String USERNAME_FIELD = "username";

    private boolean isRegistered;
    private Map<String, String> errors      = new HashMap<String, String>();
    
    private UserBean userBean;
    
    public InscriptionForm(UserBean u){
    	this.userBean =u;
    }
    public boolean getResultat() {
        return isRegistered;
    }

    public Map<String, String> getErrors() {
        return errors;
    }

    /** Procedure d'enregistrement d'un nouvel utilisateur.
     * Pour la méthode preparant l'objet user qui sera persisté, @see createNewuser() 
     * @throws UnsupportedEncodingException 
     * @throws NoSuchAlgorithmException **/
    public User registerUser( HttpServletRequest request ) throws NoSuchAlgorithmException, UnsupportedEncodingException {
        String email = getFieldValue( request, EMAIL_FIELD );
        String password = getFieldValue( request, PASSWORD_FIELD );
        String confirmation = getFieldValue( request, VERIF_FIELD );
        String username = getFieldValue( request, USERNAME_FIELD );
        
        //verification sur les parametres
        try { isCorrectFormatEmail( email ); } 
        catch ( Exception e ) { setErrors( EMAIL_FIELD, e.getMessage() ); }
        
        try { isCorrectPasssword( password, confirmation ); } 
        catch ( Exception e ) {
            setErrors( PASSWORD_FIELD, e.getMessage() );
            setErrors( VERIF_FIELD, null );
        }

        try { isCorrectFormatUsername( username );
        } catch ( Exception e ) { setErrors( USERNAME_FIELD, e.getMessage() ); }
        
        User user = null;
        if ( errors.isEmpty() ) {
            isRegistered = true;
			user = createNewUser(username, password, email);
        } else {
            isRegistered = false;
        }

        return user;
    }
    
    /*
     * Methode preparant le nouvel utilisateur avant qu'il soit persisté.
     */
    private User createNewUser(String username, String password, String email) throws NoSuchAlgorithmException, UnsupportedEncodingException{
    	User user = new User();
    	
    	//traitement du username pour en faire un slug
    	Pattern NONLATIN = Pattern.compile("[^\\w-]");  
    	Pattern WHITESPACE = Pattern.compile("[\\s]");  
    	username = WHITESPACE.matcher(username).replaceAll("-");  
    	username = Normalizer.normalize(username, Form.NFD);  
	    username = NONLATIN.matcher(username).replaceAll("");  
	    username =  username.toLowerCase(Locale.ENGLISH);  
    	
    	
    	user.setUsername(username);
    	user.setUsernameCanonical(username);
    	user.setEmail(email);
    	user.setEmailCanonical(email);
    	user.setEnabled(false);
    	
    	//generation du salt
    	SecureRandom random = new SecureRandom();
    	user.setSalt(new BigInteger(130, random).toString(10));
    	user.setPassword( user.hash(user.getSalt(), password) );
    	user.setLastLogin(null);
    	user.setLocked(false);
    	user.setExpired(false);
    	user.setExpiresAt(null);
    	user.setConfirmationToken(new BigInteger(130, random).toString(20)); //token de verification.
    	user.setPasswordRequestedAt(null);
    	user.setRoles(10);
    	user.setCredentialsExpired(false);
    	user.setCredentialsExpireAt(null);
        user.setUserDetails(new UserDetails());
		return user;
    }
    
    
    private void isCorrectFormatEmail( String email ) throws Exception {
        if ( email != null ) {
            if ( !email.matches( "([^.@]+)(\\.[^.@]+)*@([^.@]+\\.)+([^.@]+)" ) ) {
                throw new Exception( "Adresse mail invalide." );
            }
            else{ //verification de l'unicité du username
            	User userTest = userBean.findByEmail(email);
            	if(userTest != null){
            		throw new Exception("Cet email est déjà pris.");
            	}
            }
        } else {
            throw new Exception( "Adresse mail non renseignée." );
        }
    }

    private void isCorrectPasssword( String password, String confirmation ) throws Exception {
        if ( password != null && confirmation != null ) {
            if ( !password.equals( confirmation ) ) {
                throw new Exception( "Les mots de passe entrés sont différents, merci de les saisir à nouveau." );
            } else if ( password.length() < 3 ) {
                throw new Exception( "Les mots de passe doivent contenir au moins 3 caractères." );
            }
        } else {
            throw new Exception( "Merci de saisir et confirmer votre mot de passe." );
        }
    }

    private void isCorrectFormatUsername( String name ) throws Exception {
        if ( name != null && name.length() < 3 ) {
            throw new Exception( "Le nom d'utilisateur doit contenir au moins 3 caractères." );
        }
        else{ //verification de l'unicité du username
        	User userTest = userBean.findByUsername(name);
        	if(userTest != null){
        		throw new Exception("Ce username est déjà pris.");
        	}
        }
    }

    /*
     * Ajoute un message correspondant au champ spécifié à la map des erreurs.
     */
    private void setErrors( String champ, String message ) {
        errors.put( champ, message );
    }

    /*
     * Méthode utilitaire qui retourne null si un champ est vide, et son contenu
     * sinon.
     */
    private static String getFieldValue( HttpServletRequest request, String fieldName ) {
        String value = request.getParameter( fieldName );
        if ( value == null || value.trim().length() == 0 ) {
            return null;
        } else {
            return value.trim();
        }
    }
    
    
    
}