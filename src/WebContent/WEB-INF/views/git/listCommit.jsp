<%@page import="com.salescode.entity.Repository,org.eclipse.jgit.revwalk.RevCommit, java.lang.Iterable" %>
<%@ page pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<h3>List des commits</h3>

<table class="table table-striped">
<%
int count = 0;
Iterable<RevCommit> commits = (Iterable<RevCommit>) request.getAttribute("listCommit");
for (RevCommit commit : commits) {
	%>
     <tr>
        <td><%= commit.getFullMessage() %></td>  
        <td><%=commit.getShortMessage() %></td>  
        <td><%=commit.getCommitTime() %></td>  
    </tr>
	<%
    count++;
}
%>
</table>
