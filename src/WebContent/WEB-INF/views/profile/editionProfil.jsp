<%@page import="com.salescode.form.ProfilForm"%>
<%@ page pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<title>Edition profil</title>
<jsp:include page="../header.jsp">
	<jsp:param name="title" value="Editer son Profil" />
</jsp:include>
<div class="container">
	<h1>Edition du profil</h1>
	<hr>
		<% 	ProfilForm profileForm = (ProfilForm) request.getAttribute("form");
    if( request.getAttribute("message") != null || profileForm != null ){ %>
      <div class="alert alert-${empty form.errors ? 'success' : 'danger'}">
			<c:forEach items="${form.errors}" var="erreur">
		   		${erreur} &nbsp; &#124;
			</c:forEach>
			${message}
	</div>
	<% } %>
	<div class="row">


		<h3>Informations personnelles</h3>


		<form class="form-horizontal" role="form" method="POST" action="profile">
			<div class="form-group">
				<label class="col-lg-3 control-label">Nom:</label>

				<div class="col-lg-8">
					<input class="form-control" value="${user.userDetails.name}"
						type="text" id="name" name="name">
				</div>
			</div>
			<div class="form-group">

				<label class="col-lg-3 control-label">Prenom:</label>
				<div class="col-lg-8">
					<input class="form-control" value="${user.userDetails.surname}"
						type="text" id="surname" name="surname">
				</div>
			</div>
			<div class="form-group">
				<label class="col-lg-3 control-label">Date d'anniversaire (jj/mm/aaaa):</label>
				<div class="col-lg-8">
					<input class="form-control" value="${user.userDetails.birthdate }"type="text" id="birthdate" name="birthdate">
				</div>
			</div>
			<div class="form-group">
				<label class="col-lg-3 control-label">Description:</label>
				<div class="col-lg-8">
					<input class="form-control" value="${user.userDetails.description}"
						type="text" id="description" name="description">
				</div>
			</div>
			<div class="form-group">
				<label class="col-lg-3 control-label">Ville:</label>
				<div class="col-lg-8">
					<input class="form-control" value="${user.userDetails.city}"
						type="text" id="city" name="city">
				</div>
			</div>
			<div class="form-group">
				<label class="col-lg-3 control-label">Pays:</label>
				<div class="col-lg-8">
					<input class="form-control" value="${user.userDetails.country}"
						type="text" id="country" name="country">
				</div>
			</div>
			<div class="form-group">
				<label class="col-lg-3 control-label">Email:</label>
				<div class="col-lg-8">
					<input class="form-control" value="${user.email}" type="text" id="email" name="email">
				</div>
			</div>
			<div class="form-group">
				<label class="col-lg-3 control-label">Avatar:</label>
				<div class="col-lg-8">
					<input class="form-control" value="${user.userDetails.avatar}" type="text" id="avatar" name="avatar">
				</div>
			</div>
			<div class="text-center">
				<img src="${user.userDetails.avatar}" class="avatar img-circle" width="150px" height="150px">
			</div>
			<div class="form-group">
				<label class="col-md-3 control-label"></label>
				<div class="col-md-8">
					<input class="btn btn-primary" value="Modifier" type="submit">
				</div>
			</div>
		</form>
	</div>
</div>
<hr>

<jsp:include page="../footer.jsp"></jsp:include>

</body>
</html>