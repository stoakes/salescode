<%@ page pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<title>Affichage de votre profil</title>
<jsp:include page="../header.jsp">
	<jsp:param name="title" value="${user.username }" />
</jsp:include>
<div class="container">
	<div class="row">
		<div class="col-md-5  toppad  pull-right col-md-offset-3 ">
			<a href="/salescode/profile?op=edit" class="btn btn-default">Editer le
				Profil</a> <br>
		</div>
		<div
			class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xs-offset-0 col-sm-offset-0 col-md-offset-3 col-lg-offset-3 toppad">


			<div class="panel panel-info">
				<div class="panel-heading">
					<h3 class="panel-title">${user.userDetails.name}
						${user.userDetails.surname}</h3>
				</div>
				<div class="panel-body">
					<div class="row">
						<div class="col-md-3 col-lg-3 " align="center">
							<img src="${user.userDetails.avatar}"
								class="img-circle img-responsive">
						</div>


						<div class=" col-md-9 col-lg-9 ">
							<table class="table table-user-information">
								<tbody>
									<tr>

										<td>Anniversaire</td>

										<td>${user.userDetails.birthdate}</td>
									</tr>
									<tr>
										<td>Description</td>
										<td>${user.userDetails.description}</td>
									</tr>

									<tr>
									<tr>

										<td>Adresse (ville, pays)</td>
										<td>${user.userDetails.city}${userD.country}</td>
									</tr>
									<tr>
										<td>Email</td>
										<td>${user.email}</td>
									</tr>

								</tbody>
							</table>
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>
</div>

<jsp:include page="../footer.jsp"></jsp:include>

</body>
</html>