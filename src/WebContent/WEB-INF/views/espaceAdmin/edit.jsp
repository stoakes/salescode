<jsp:include page="../header.jsp">
	<jsp:param name="title">
		<jsp:attribute name="value">
       Editer les droits
      </jsp:attribute>
	</jsp:param></jsp:include>

<form method="POST" action="EspaceAdmin">
	<h2>${user.username }</h2>
	<br /> Activ� <input type="radio" name="enabled" value="1"
		${user.enabled ? 'checked': '' }> &nbsp; &#124; &nbsp; Email a confirmer <input type="radio" name="enabled" value="2"
		${user.enabled ? '': 'checked' }> <br />
	<br /> Banni <input type="radio" name="locked" value="1"
		${user.locked ? 'checked': '' }> &nbsp; &#124; &nbsp; Valide <input type="radio" name="locked" value="2"
		${user.locked ? '': 'checked' }> <br />
	<br /> <input type="submit" value="Enregistrer" class="btn btn-success">
</form>

<jsp:include page="../footer.jsp" />
