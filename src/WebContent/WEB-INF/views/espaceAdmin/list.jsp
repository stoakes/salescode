<%@page import="com.salescode.entity.User, java.util.List" %>
<%@ page pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<jsp:include page="../header.jsp">
	<jsp:param name="title">
		<jsp:attribute name="value">
       Afficher droits users
      </jsp:attribute>
	</jsp:param></jsp:include>
<h2>Espace d'administration</h2>
<table class="table table-striped table-hover">
	<tr>
		<th>Utilisateur</th>
		<th>Activé</th>
		<th>Verrouillé</th>
		<th>Editer</th>
	</tr>

	<c:forEach items="${userList}" var="user">
		<tr>
			<td>${user.username }</td>
			<th>${user.enabled}</th>
			<th>${user.locked}</th>
			<th><a href="?id=${user.id}"> Editer ${user.username }</a></th>
		</tr>
	</c:forEach>
</table>

<jsp:include page="../footer.jsp"></jsp:include>
</body>
</body>
</html>