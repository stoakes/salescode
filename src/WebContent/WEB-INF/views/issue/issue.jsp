<%@ page pageEncoding="UTF-8" %>
<%@page import="com.salescode.entity.User, com.salescode.entity.Issue, java.util.List" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="../header.jsp">
  <jsp:param name="title" >
      <jsp:attribute name="value" >
       Issue $ ${issue.repository.creator.username } - ${issue.repository.title }
      </jsp:attribute>
  </jsp:param>
</jsp:include>
	<h1>Issue #${issue.id }</h1>
	<h3>${issue.title }</h3>
			
			<div class="timeline-container">
                <ul class="timeline" id="issuesCommentaryList">
                	<!--  premier message écrit en dur -->
                	<li>
                     	<i class="fa fa-comments bg-yellow"></i>
						<div class="timeline-item">
		                    <span class="time"><i class="fa fa-clock-o"></i> ${issue.createdOn }</span>

                            <h3 class="timeline-header"><a href="profile?id=${issue.creator.id}">${issue.creator.username}</a></h3>
							<div class="timeline-body">
								${issue.message }
                            </div>
                           
                         </div>
                    </li>
                                    
                       <c:forEach items="${issue.commentaries}" var="comment">
							<li>
					        <i class="fa fa-comments bg-yellow"></i>
								<div class="timeline-item">
					              <span class="time"><i class="fa fa-clock-o"></i> ${comment.createdOn }</span>
									<h3 class="timeline-header"><a href="profile?id=${comment.creator.id}">${comment.creator.username}</a></h3>
									<div class="timeline-body">
										${comment.message }
					                </div>
						                       
					            </div>
					       </li>
						</c:forEach>
					<% 
					User user = (User) request.getSession().getAttribute("user");
					Issue issue = (Issue) request.getAttribute("issue");
					
					if(issue != null && issue.isClosed()){ %>
						<li>
					        <i class="fa fa-times bg-red"></i>
								<div class="timeline-item">
					              <span class="time"><i class="fa fa-clock-o"></i> ${issue.closedOn }</span>
									<h3 class="timeline-header"><a href="profile?id=${issue.closeUser.id }">${issue.closeUser.username }</a></h3>
									<div class="timeline-body">
										Issue fermé le ${issue.closedOn } par <a href="profile?id=${issue.closeUser.id }">${issue.closeUser.username }</a>.
					                </div>
						                       
					            </div>
					       </li>
					<%}  %>
					
					
					<%
						if(user != null){ %>
								<div id="loader" style="display: none">
									<img src="/salescode/assets/images/loader.gif" />
								</div>
								<div id="resultatAjax"></div>
								

								
					<li>
                        <i class="fa fa-comments bg-yellow"></i>

                        <div class="timeline-item">
                            

                            <h3 class="timeline-header"><a href="profile?id=${user.id}">${user.username}</a></h3>

                            <div class="timeline-body">
                                <form action="" method="POST" id=newIssueCommentary>
									<textarea name="message" cols="100" rows="10" id="message"></textarea>
									<input type="hidden" name="issueMaster" value="${issue.id }">
									<input type="hidden" name="action" value="post">
									<input type="submit" value="Poster un nouveau commentaire sur cet issue" class="btn btn-default">
								</form>
                            </div>
                            <%
								if( (user.getId() == issue.getCreator().getId() || user.getId() == issue.getRepository().getCreator().getId()) && !issue.isClosed() ){ // si on a affaire au créateur de l'issue ou du repo, on lui offre la possibilité de fermer l'issue
								%>
								<div class="timeline-footer" >
									<form action="" method="POST" id="closeIssue">
										<input type="hidden" name="id" value="${issue.id }" />
										<input type="hidden" name="action" value="close" /><!--  permet que ca fonctionne meme sans JS activé ;) -->
										<input type="submit" class="btn btn-success" value="Clore l'issue" id="closeButton" />
									</form>
									</div >
								
								<% } %>
								</div>
								</li>
						<%
						}else{ %>
						<li>
	                        <i class="fa fa-lock bg-blue"></i>
							<div class="timeline-item">
	                           <div class="timeline-body">
	                                Connectez vous pour répondre à cet issue
	                            </div>
	                        </div>
                        </li>
					<% } %>
		
				</ul>
			</div>


	
	
	<a href="/salescode/r/${repository.creator.username }/${repository.title }/issues/">Liste des issues sur ce depot</a>

<script type="text/javascript">
$(function () {
    $("#newIssueCommentary").submit(function () {
        
		message = $('textarea#message').val();
		issueMaster = $(this).find("input[name=issueMaster]").val();

		//validation des données postées :
		if(message.length  < 6) { 
			$("#resultatAjax").hide().append('<div class="alert alert-warning">Le message est trop court.</div>').slideDown(500);
			return false;
		}

		$("#loader").show();
		url = window.location.href;
    	 $.post(url, {
            message: message,
            issueMaster: issueMaster,
            action: "post"
        }, function (data) {
            console.log(data);
            //data = data;
            $("#loader").hide();
            $("#resultatAjax").hide().append('<div class="alert alert-'+data["message-class"]+'">'+data["message"]+'</div>').slideDown(500);


            if(data["message-class"].localeCompare("success") == 0){//form validé avec succès
                $("#message").val();
				$("#issuesCommentaryList").append('<li><i class="fa fa-comments bg-blue"></i><div class="timeline-item"><div class="timeline-body">'+message+'</div></div></li>').show();
            }

        });
        return false;
    });
});

$(function () {
    $("#closeIssue").submit(function () {
        
		id = $(this).find("input[name=id]").val();	

		$("#loader").show();
		url = window.location.href;
    	 $.post(url, {
            id: id,
            action: "close"
        }, function (data) {
            $("#loader").hide();
            $("#resultatAjax").hide().append('<div class="alert alert-'+data["message-class"]+'">'+data["message"]+'</div>').slideDown(500);


            if(data["message-class"].localeCompare("success") == 0){//form validé avec succès
                $("#closeButton").addClass("disabled");
            }

        });
        return false;
    });
});

</script>
<jsp:include page="../footer.jsp"></jsp:include>