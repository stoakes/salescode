<%@page import="com.salescode.entity.Repository, com.salescode.entity.Issue, java.util.List" %>
<%@ page pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<jsp:include page="../header.jsp">
  <jsp:param name="title" >
      <jsp:attribute name="value" >
          ${repository.title } - Liste des Issues
      </jsp:attribute>
  </jsp:param>
</jsp:include>
 
 <h3> ${repository.title } - Nouvel issue</h3>

<div id="loader" style="display: none">
	<img src="/salescode/assets/images/loader.gif" />
</div>
<div id="resultatAjax">
</div>
<form action="" method="POST" id="newIssue">
	<input type="text" name="title" id="title" /><div id="title-text"></div><br/>
	<textarea name="message" cols="100" rows="10" id="message"></textarea><br/>
	<div id="message-text" class="well" style="display: none"></div>
	
	<input type="hidden" name="action" value="new" />
	<input type="submit" value="Creer" class="btn btn-success" id="submitButton">
</form>
<a href="..">Retour au dépot</a>


<script type="text/javascript">
$(function () {
    $("#newIssue").submit(function () {
        
        title = $(this).find("input[name=title]").val();
		message = $('textarea#message').val();

		//validation des données postées :
		if(title.length  < 6) { 
			$("#resultatAjax").hide().append('<div class="alert alert-warning">Le titre est trop court.</div>').slideDown(500);
			return false;
		}
		if(message.length  < 6) { 
			$("#resultatAjax").hide().append('<div class="alert alert-warning">Le message est trop court.</div>').slideDown(500);
			return false;
		}

		$("#loader").show();
		url = window.location.href;
    	 $.post(url, {
            title: title,
            message: message,
            action: "new"
        }, function (data) {
            console.log(data);
            //data = data;
            $("#loader").hide();
            $("#resultatAjax").hide().append('<div class="alert alert-'+data["message-class"]+'">'+data["message"]+'</div>').slideDown(500);


            if(data["message-class"].localeCompare("success") == 0){//form validé avec succès
				$("#message").hide();	
				$("#submitButton").hide();	
				$("#title").hide();
				$("#message-text").html(message).show();
				$("#title-text").html('<h3>'+title+'</h3>').show();
            }

        });
        return false;
    });
});



</script>

<jsp:include page="../footer.jsp"></jsp:include>