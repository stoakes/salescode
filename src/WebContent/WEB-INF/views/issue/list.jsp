<%@page import="com.salescode.entity.Repository, com.salescode.entity.Issue, java.util.List" %>
<%@ page pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<jsp:include page="../header.jsp">
  <jsp:param name="title" >
      <jsp:attribute name="value" >
          ${repository.title } - Liste des Issues
      </jsp:attribute>
  </jsp:param>
</jsp:include>
 
 <h1>${repository.title }</h1>
<h3>${repository.description }</h3>
	
	<div class="container-fluid">
          <div class="nav-tabs-custom">
              <ul class="nav nav-tabs" id="myTabs">
                  <li>
                  <a href="/salescode/r/${repository.creator.username }/${repository.title}/" >General</a>
                  </li>
                  <li class="active"><a href="/salescode/r/${repository.creator.username }/${repository.title}/issues/">Issues</a></li>
                  <li><a href="/salescode/r/${repository.creator.username }/${repository.title }/issues/new" >Nouvel Issue</a></li>
              </ul>
          </div>
          <div class="tab-content">
              <div class="tab-pane active" id="general">
              	
              	<table class="table table-striped">
					<tr>
						<th>Nom</th>
						<th>Description</th>
						<th>Createur</th>
					</tr>
					<c:forEach items="${issueList}" var="issue">
					    <tr>
					        <td><a href="${issue.id}">${issue.title}</a></td>  
					        <td>${issue.message}</td>  
					        <td><a href="profile?id=${issue.creator.id}">${issue.creator.username}</a></td>
					    </tr>
					</c:forEach>
				</table>
              	
              </div>
              
          </div>
         </div>

 


<jsp:include page="../footer.jsp"></jsp:include>