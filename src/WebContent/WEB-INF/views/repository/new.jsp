<%@ page pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="../header.jsp">
  <jsp:param name="title" >
      <jsp:attribute name="value" >
        Créez un nouveau Depot
      </jsp:attribute>
  </jsp:param>
</jsp:include>
	<div class="container">
    <h1 class="well">Ajout Repository</h1>
	<div class="col-lg-12 well">
	<div class="row">
				<form method="POST" action="/salescode/repositories?op=new">
					<div class="col-sm-12">
						<div class="row">
							<div class="col-sm-6 form-group">
								<label>Titre</label>
								<input type="text" name="title" placeholder="Titre" class="form-control">
							</div>
							<div class="col-sm-6 form-group">
								<label>Prix</label>
								<input type="number"  step="0.01" name="price" placeholder="Prix" class="form-control">
							</div>
						</div>					
						<div class="form-group">
							<label>Description</label>
							<textarea placeholder="Description" name="description" rows="3" class="form-control"></textarea>
						</div>	
					<div class="form-group">
						<label>Lien de l'image</label>
						<input type="text" name="img" placeholder="URL image" class="form-control">
					</div>
					<input type="hidden" name="op" value="new" />
					<button type="submit" class="btn btn-lg btn-info">Ajouter</button>					
					</div>
				</form> 
				</div>
	</div>
	</div>

<jsp:include page="../footer.jsp"></jsp:include>