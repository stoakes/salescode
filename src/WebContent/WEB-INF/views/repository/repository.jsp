<%@ page pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="../header.jsp">
  <jsp:param name="title" >
      <jsp:attribute name="value" >
        ${repository.creator.username } - ${repository.title }
      </jsp:attribute>
  </jsp:param>
</jsp:include>
	<h1>${repository.title }</h1>
	<h3>${repository.description }</h3>
			
		<div class="container-fluid">
          <div class="nav-tabs-custom">
          
              <ul class="nav nav-tabs" id="myTabs">
                  <li class="active">
                  <a href="#general" class="link"
                                        data-url="/salescode/Git?creator=${repository.creator.username }&op=${repository.title}&folder=">General</a>
                  </li>
                  <li><a href="/salescode/r/${repository.creator.username }/${repository.title}/issues/">Issues</a></li>
                  	<li><a href="#achat" class="link" data-toggle="tab" >Acheter</a></li>
              </ul>
          </div>
          <div class="tab-content">
              <div class="tab-pane active" id="general"></div>
               
              <div class="tab-pane" id="achat">
       
					<div class="thumbnail">
						<img class="group list-group-image" src="${repository.image }"  alt="${repository.title }" />
						<div class="caption">
						    <h4 class="group inner list-group-item-heading">Acheter ${repository.title}</h4>
						<p class="group inner list-group-item-text">
						    ${repository.description}</p>
						<div class="row">
						    <div class="col-xs-12 col-md-6">
						        <p class="lead">
						            ${repository.price} &euro;</p>
						</div>
						<div class="col-xs-12 col-md-6">
						<% if(request.getSession().getAttribute("user") != null){ //s'il est connecté. %>
						    <a class="btn btn-success" href="/salescode/ShoppingCart?product=${repository.id}">Add to cart</a>
						    <% }
						else{%>
							<h4><a href="/salescode/connexion">Connectez vous pour acheter ${repository.title }</a></h4>
						<% } %>
						            </div>
						        </div>
						    </div>
					</div>
              	
              </div>
              
          </div>
         </div>


	
	
<script type="text/javascript">
    jQuery(document).ready(function ($) {
        $('#myTabs').tab();
    });
</script> 
	 <script type="text/javascript">
            $(document).ready(function(){
            	$('#general').load($('.active a').attr("data-url"), function (result) {
                    $('.active a').tab('show');
                });
            });
        </script>
	
	<script>
		//C'est un peu la fete du hack ce truc :
		var path = "${path}";
	</script>

<jsp:include page="../footer.jsp"></jsp:include>