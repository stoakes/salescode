<%@ page pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<jsp:include page="header.jsp">
  <jsp:param name="title" >
      <jsp:attribute name="value" >
         Connexion
      </jsp:attribute>
  </jsp:param>
</jsp:include>
<div class="container">
    <div class="row">
        <div class="col-sm-6 col-md-4 col-md-offset-4">
            <h1 class="text-center login-title">Connectez vous pour continuer!</h1>
            ${empty message  ? '':  '<div class="alert alert-danger">'.concat(message).concat('</div>') }
            <div class="account-wall">
                <form class="form-signin" method="post" action="connexion">
                <input type="text" class="form-control" id="email" name="email" value="${utilisateur.email}" placeholder="Email" required autofocus>
                <span class="erreur">${form.errors['email']}</span>
                <input type="password" class="form-control" id="motdepasse" name="motdepasse" placeholder="Password" required>
                <button class="btn btn-lg btn-primary btn-block" type="submit" value="Connexion" >
                    Connexion</button>              
                </form>
            </div><br/>
            <a href="/salescode/inscription" class="text-center new-account">Creer un compte </a>
        </div>
    </div>
</div>

<jsp:include page="footer.jsp"></jsp:include>