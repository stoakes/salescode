 <!-- Left side column. contains the logo and sidebar -->
      <aside class="main-sidebar">

        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
          <!-- search form (Optional) -->
          <form action="/salescode/search" method="post" class="sidebar-form">
            <div class="input-group">
              <input type="text" name="search" class="form-control" placeholder="Search..."/>
              <span class="input-group-btn">
                <button type='submit' class="btn btn-flat"><i class="fa fa-search"></i></button>
              </span>
            </div>
          </form>
          <!-- /.search form -->

          <!-- Sidebar Menu -->
          <ul class="sidebar-menu">
           <!-- <li class="header">HEADER</li> -->
           <% if(request.getSession().getAttribute("user") != null){ %>
           		<li>
					<a href="/salescode/"><i class="fa fa-home"></i> <span class="nav-label">Index</span></a>
				</li>
				<li>
					<a href="/salescode/dashboard"><i class="fa fa-th-large"></i> <span class="nav-label">Dashboard</span></a>
				</li>
				<li>
					<a href="/salescode/marketplace"><i class="fa fa-shopping-cart"></i> <span class="nav-label">Marketplace</span></a>
				</li>
				<li>
					<a href="/salescode/repositories?op=new"><i class="fa fa-folder-open"></i> <span class="nav-label">Ajout Repository</span></a>
				</li>
				<li>
					<a href="/salescode/ShoppingCart"><i class="fa fa-calendar-o"></i> <span class="nav-label">Mon Panier</span></a>
				</li>
				<li>
					<a href="/salescode/bill"><i class="fa fa-eur"></i> <span class="nav-label">Mes factures</span></a>
				</li>
				<li>
					<a href="/salescode/member"><i class="fa fa-users"></i> <span class="nav-label">Membres</span></a>
				</li>

				<%} else { %>
				<li>
					<a href="/salescode/marketplace"><i class="fa fa-shopping-cart"></i> <span class="nav-label">Marketplace</span></a>
				</li>
				<li>
					<a href="/salescode/inscription"><i class="fa fa-sign-in"></i> <span class="nav-label">Inscription</span></a>
				</li>
				<li>
					<a href="/salescode/connexion"><i class="fa fa-key"></i> <span class="nav-label">Connexion</span></a>
				</li>
				<%} %>
          </ul><!-- /.sidebar-menu -->
        </section>
        <!-- /.sidebar -->
      </aside>