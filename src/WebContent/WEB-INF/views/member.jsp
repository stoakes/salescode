<%@ page pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<jsp:include page="header.jsp">
	<jsp:param name="title">
		<jsp:attribute name="value">
        Membres
      </jsp:attribute>
	</jsp:param>
</jsp:include>
<div class="container bootstrap snippet">
	<div class="row">
		<div class="col-lg-12">
			<div class="main-box no-header clearfix">
				<div class="main-box-body clearfix">
					<div class="table-responsive">
						<table class="table user-list">
							<thead>
								<tr>
									<th><span>Utilisateur</span></th>
									<th><span></span></th>
									<th><span>Anniversaire</span></th>
									<th class="text-center"><span>Status</span></th>
									<th><span>Email</span></th>
									<th>&nbsp;</th>
								</tr>
							</thead>
							<tbody>
								<c:forEach items="${listUser}" var="user">
									<tr>
										<td style="cursor: pointer;"
											onclick='document.location.href="profile?id=${user.id}";'><img
											src="${user.userDetails.avatar }" alt="" width="75px"
											height="75px"></td>
										<td style="cursor: pointer;"
											onclick='document.location.href="profile?id=${user.id}";'>${user.userDetails.name}
											${user.userDetails.surname}</td>
										<td style="cursor: pointer;"
											onclick='document.location.href="profile?id=${user.id}";'>${user.userDetails.birthdate }</td>
										<c:choose>
											<c:when test="${user.roles>49}">
												<td class="text-center" style="cursor: pointer;"
													onclick='document.location.href="profile?id=${user.id}";'><span
													class="label label-danger">Admin</span></td>
											</c:when>
											<c:otherwise>
												<td class="text-center" style="cursor: pointer;"
													onclick='document.location.href="profile?id=${user.id}";'><span
													class="label label-success">Membre</span></td>
											</c:otherwise>
										</c:choose>
										<td style="cursor: pointer;"
											onclick='document.location.href="profil/${user.id}";'>${user.email}</td>
										<c:if test="${currentUser.roles>49}">
											<td style="width: 20%;"><a href="/salescode/EspaceAdmin?id=${user.id }" class="table-link">
													<span class="fa-stack"> <i
														class="fa fa-square fa-stack-2x"></i> <i
														class="fa fa-pencil fa-stack-1x fa-inverse"></i>
												</span>
											</a> <a href="#" class="table-link danger"> <span
													class="fa-stack"> <i
														class="fa fa-square fa-stack-2x"></i> <i
														class="fa fa-trash-o fa-stack-1x fa-inverse"></i>
												</span>
											</a></td>
										</c:if>
									</tr>
								</c:forEach>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<jsp:include page="footer.jsp"></jsp:include>
