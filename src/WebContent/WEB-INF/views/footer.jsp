</div>
</div> <!-- fin row -->
</section><!-- /.content -->
</div><!-- /.content-wrapper -->


<footer class="main-footer">
    <strong>Copyright &copy; 2013-2015 <a href="#">Sales Code</a>.</strong> Tous droits r&eacute;serv&eacute;s.
</footer>

</div><!-- ./wrapper -->
<%if(request.getSession().getAttribute("user") != null){ //inutile de lancer le polling s'il n'est pas d�j� connect�] %>
<script>
//va chercher toutes les 5 secondes le  nombre de notifications en cours.
$(document).ready(function doPoll(){
    $.get('/salescode/notification?action=count', function(data) {
        if(data["message"] > 0){
    		$('#dropdown-notification').append('<span class="label label-warning">'+data["message"]+'</span>');
    		$('#vous-avez-notif').text(data["message"]);
        }
        	setTimeout(doPoll,5000);
    });
});

//chargement des notifications dans le dropdwon (asynchrone <3) 
$("[data-rel='ajax-dropdown']").click(function (e) {
    e.preventDefault();
    var url = $(this).attr("data-url");
    var href = this.hash;
    var id = $(this).attr("data-target");
	
			$.get(url,function(data){ 
				console.log(data);
				$.each(data["message"], function(index, value){
					$('#'+id).prepend("<li><h4>"+value["title"]+"</h4><small><i class=\"fa fa-clock-o\"></i>"+value["createdOn"]+"</small><p>"+value["message"]+"</p></li>"); 
					});
				});
	  });  

$("#lu-notif").click(function (e){
	e.preventDefault();
	$.get("/salescode/notification?action=readAll",function(data){
		$('#dropdown-notification').html("<i class=\"fa fa-bell-o\"></i>");	
	});
});

</script>
<%} %>
</body>
</html>
