<%@page import="com.salescode.form.InscriptionForm"%>
<%@ page pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<jsp:include page="header.jsp" >
  <jsp:param name="title" value="" />
</jsp:include>

 <form class="form-horizontal" action='' method="post" action="inscription">
  <fieldset>
    <div id="legend">
      <legend class="">Inscription</legend>
    </div>
    
    <% 
    if( request.getAttribute("message") != null || request.getAttribute("form") != null  ){ %>
      <div class="alert alert-${empty form.errors ? 'success' : 'danger'}">
			<c:forEach items="${form.errors}" var="erreur">
		   		${erreur} &nbsp; &#124;
			</c:forEach>
			${message}
	</div>
	<% } %>
		
    <div class="control-group">
      <!-- Username -->
      <label class="control-label"  for="username">Username</label>
      <div class="controls">
        <input type="text" id="username" name="username" placeholder="" value="<c:out value="${utilisateur.nom}"/>" class="input-xlarge">
        <p class="help-block">Le pseudo peut contenir des lettres ou chiffres sans espaces</p>
        <span class="erreur">${form.errors['username']}</span>
      </div>
    </div>
 
    <div class="control-group">
      <!-- E-mail -->
      <label class="control-label" for="email">E-mail</label>
      <div class="controls">
        <input type="text" id="email" name="email" placeholder="" value="<c:out value="${utilisateur.email}"/>" class="input-xlarge">
        <p class="help-block">Veuillez indiquer un E-mail valide</p>
        <span class="erreur">${form.errors['email']}</span>
      </div>
    </div>
 
    <div class="control-group">
      <!-- Password-->
      <label class="control-label" for="password">Mot de passe</label>
      <div class="controls">
        <input type="password" id="password" name="password" placeholder="" class="input-xlarge">
        <p class="help-block">Le mot de passe doit comporter au moins 6 caractères.</p>
        <span class="erreur">${form.errors['password']}</span>
      </div>
    </div>
 
    <div class="control-group">
      <!-- Password -->
      <label class="control-label"  for="confirmation">Mot de passe (Confirmation)</label>
      <div class="controls">
        <input type="password" id="confirmation" name="confirmation" value="" placeholder="" class="input-xlarge">
        <p class="help-block">Confirmez le mot de passe</p>
         <span class="erreur">${form.errors['confirmation']}</span>
      </div>
    </div>
 
    <div class="control-group">
      <!-- Button -->
      <div class="controls">
        <button class="btn btn-success" value="Inscription">S'inscrire</button>
      </div>
    </div>
  </fieldset>
</form>
<jsp:include page="footer.jsp" ></jsp:include>