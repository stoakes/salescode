<%@page import="com.salescode.entity.Repository, java.util.List" %>
<%@ page pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<jsp:include page="header.jsp">
  <jsp:param name="title" >
      <jsp:attribute name="value" >
         Tableau de bord
      </jsp:attribute>
  </jsp:param>
</jsp:include>
 
 Username : ${sessionScope.user.username}

 <div class="row">
 <div class="col-xs-12 col-sm-6">
	 <h3> Vos depots</h3>
 </div>
 <div class="col-xs-12 col-sm-6">
 <span class="pull-right">
	 <a href="/salescode/repositories?op=new" class="btn btn-info">Nouveau Depot</a>
 </span>
 </div>
<table class="table table-striped">
<tr>
	<th>Nom</th>
	<th>Description</th>
	<th>Path</th>
	<th>Prix</th>
	<th>Ventes</th>
</tr>
<c:forEach items="${persoRepositories}" var="repo">
    <tr>
        <td><a href="/salescode/r/${repo.creator.username}/${repo.title}/">${repo.title}</a></td>  
        <td>${repo.description}</td>  
        <td>${repo.path}</td>
        <td>${repo.price} &euro;</td>
        <td>${ repo.sales}</td>
    </tr>
</c:forEach>
</table>

				
			
<jsp:include page="footer.jsp"></jsp:include>