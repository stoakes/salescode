<%@page import="com.salescode.entity.User"%>
<nav class="navbar navbar-static-top" role="navigation">

	<!-- Sidebar toggle button-->
	<a href="#" class="sidebar-toggle" data-toggle="offcanvas"
		role="button"> <span class="sr-only">Toggle navigation</span>
	</a>
	<!-- Navbar Right Menu -->
	<div class="navbar-custom-menu">

		<ul class="nav navbar-nav">
			<%
				User user = (User) session.getAttribute("user");
				if (user != null && user.getRoles() > 49) {
			%>
			<li class="grey"><a data-toggle="dropdown"
				class="dropdown-toggle" href="#"><i class="ace-icon fa fa-tasks"></i></a>

				<ul
					class="pull-right dropdown-navbar dropdown-menu dropdown-caret dropdown-close">
					<li class="dropdown-header"><i class="fa fa-ok"></i>
						Administration</li>

					<li><a href="/salescode/EspaceAdmin"><div class="clearfix">
								<span class="pull-left">Espace d'administration de droits</span>
							</div></a></li>
					<!--<div class="progress progress-mini ">
								<div style="width:65%" class="progress-bar "></div>
								</div>-->
				</ul></li>

			<!-- Notifications Menu -->
			<li class="dropdown notifications-menu">
				<!-- Menu toggle button --> <a href="#" class="dropdown-toggle"
				data-toggle="dropdown"
				data-url="/salescode/notification?action=listAll"
				data-rel="ajax-dropdown"
				data-target="dropdown-notification-container"
				id="dropdown-notification"> <i class="fa fa-bell-o"></i>
			</a>
				<ul class="dropdown-menu" id="dropdown-notification-container">
					<li class="header">Vous avez <span id="vous-avez-notif"></span>
						notifications
					</li>
					<li class="footer"><a href="#" id="lu-notif">Marquer comme
							lu</a></li>
				</ul>
			</li>
			<!-- User Account Menu -->
			<li class="dropdown user user-menu">
				<!-- Menu Toggle Button --> <a href="#" class="dropdown-toggle"
				data-toggle="dropdown"> <!-- The user image in the navbar--> <img
					src="${sessionScope.user.userDetails.avatar}" class="user-image" />
					<!-- hidden-xs hides the username on small devices so only the image appears. -->
					<span class="hidden-xs">${sessionScope.user.username}</span>
			</a>
				<ul class="dropdown-menu">
					<!-- The user image in the menu -->
					<li class="user-header"><img
						src="${sessionScope.user.userDetails.avatar}" class="img-circle" />
						<p>${sessionScope.user.email}</p></li>
					<!-- Menu Body -->
					<li class="user-body">
						<div class="col-xs-4 text-center">
							<a href="/salescode/profile?op=edit">Paramètres</a>
						</div>
						<div class="col-xs-4 text-center">
							<!-- <a href="#">Sales</a>-->
						</div>

					</li>
					<!-- Menu Footer-->
					<li class="user-footer">
						<div class="pull-left">
							<a href="/salescode/profile" class="btn btn-default btn-flat">Profil</a>
						</div>
						<div class="pull-right">
							<a href="/salescode/connexion?op=sd"
								class="btn btn-default btn-flat"> <i class="fa fa-sign-out"></i>
								Deconnexion
							</a>
						</div>
					</li>
				</ul>
			</li>
			<!-- Control Sidebar Toggle Button -->
			<%
				} else if(user != null && user.getRoles()<=49)  {
					
			%>
			

			<!-- Notifications Menu -->
			<li class="dropdown notifications-menu">
				<!-- Menu toggle button --> <a href="#" class="dropdown-toggle"
				data-toggle="dropdown"
				data-url="/salescode/notification?action=listAll"
				data-rel="ajax-dropdown"
				data-target="dropdown-notification-container"
				id="dropdown-notification"> <i class="fa fa-bell-o"></i>
			</a>
				<ul class="dropdown-menu" id="dropdown-notification-container">
					<li class="header">Vous avez <span id="vous-avez-notif"></span>
						notifications
					</li>
					<li class="footer"><a href="#" id="lu-notif">Marquer comme
							lu</a></li>
				</ul>
			</li>
			<!-- User Account Menu -->
			<li class="dropdown user user-menu">
				<!-- Menu Toggle Button --> <a href="#" class="dropdown-toggle"
				data-toggle="dropdown"> <!-- The user image in the navbar--> <img
					src="${sessionScope.user.userDetails.avatar}" class="user-image" />
					<!-- hidden-xs hides the username on small devices so only the image appears. -->
					<span class="hidden-xs">${sessionScope.user.username}</span>
			</a>
				<ul class="dropdown-menu">
					<!-- The user image in the menu -->
					<li class="user-header"><img
						src="${sessionScope.user.userDetails.avatar}" class="img-circle" />
						<p>${sessionScope.user.email}</p></li>
					<!-- Menu Body -->
					<li class="user-body">
						<div class="col-xs-4 text-center">
							<a href="/salescode/profile?op=edit">Paramètres</a>
						</div>
						<div class="col-xs-4 text-center">
							<!-- <a href="#">Sales</a>-->
						</div>

					</li>
					<!-- Menu Footer-->
					<li class="user-footer">
						<div class="pull-left">
							<a href="/salescode/profile" class="btn btn-default btn-flat">Profil</a>
						</div>
						<div class="pull-right">
							<a href="/salescode/connexion?op=sd"
								class="btn btn-default btn-flat"> <i class="fa fa-sign-out"></i>
								Deconnexion
							</a>
						</div>
					</li>
				</ul>
			</li>
			<%
				}else{
			%>
			<li><a href="/salescode/connexion">Connexion</a></li>
			<li><a href="/salescode/inscription">Inscription</a></li>
			<li><a href="/salescode/">Accueil</a></li>
			<%
				}
			%>
		</ul>
	</div>
</nav>
</header>