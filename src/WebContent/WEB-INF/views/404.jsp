<%@page import="com.salescode.entity.Repository, java.util.List" %>
<%@ page pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<jsp:include page="header.jsp">
  <jsp:param name="title" >
      <jsp:attribute name="value" >
         Page introuvable
      </jsp:attribute>
  </jsp:param>
</jsp:include>
 
<h1>La page que vous recherchez n'existe pas.</h1>
				
			
<jsp:include page="footer.jsp"></jsp:include>