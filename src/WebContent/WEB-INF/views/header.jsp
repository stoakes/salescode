<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- Font Awesome Icons -->
    <link async href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!-- Theme style -->
		<link  rel="stylesheet" href="/salescode/assets/css/bootstrap.min.css" type="text/css" media="print, projection, screen" />
		<link  rel="stylesheet" href="/salescode/assets/css/style.css" type="text/css" media="print, projection, screen" />
		<link  rel="stylesheet" href="/salescode/assets/css/green.css" type="text/css" media="print, projection, screen" />
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <title>${!empty title ? title : 'SalesCode'}</title>
    	<script  type="text/javascript" src="/salescode/assets/js/jquery.min.js" ></script>
	<script  type="text/javascript" src="/salescode/assets/js/bootstrap.min.js" ></script>
  </head>

  <body class="skin-green sidebar-mini">
    <div class="wrapper">

      <!-- Main Header -->
      <header class="main-header">

        <!-- Logo -->
        <a href="/salescode/dashboard" class="logo">
          <!-- mini logo for sidebar mini 50x50 pixels -->
          <span class="logo-mini"><b>SC</b></span>
          <!-- logo for regular state and mobile devices -->
          <span class="logo-lg">SalesCode</span>
        </a>

        <!-- Header Navbar -->
		<jsp:include page="navbar-top.jsp" />
		<jsp:include page="navbar-left.jsp" />
        

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Main content -->
        <section class="content">
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">