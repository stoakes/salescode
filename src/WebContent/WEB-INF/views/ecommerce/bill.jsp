<%@ page pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="../header.jsp">
  <jsp:param name="title" >
      <jsp:attribute name="value" >
        Mes Factures
      </jsp:attribute>
  </jsp:param>
</jsp:include>
 <table class = "table">
   <caption>Mes Factures</caption>
   
   <thead>
      <tr>
         <th>Numéro de Facture</th>
         <th>Date de Paiement</th>
         <th>Prix</th>
      </tr>
   </thead>
   
   <tbody>
   <c:forEach items="${listBill}" var="bill" >

     <tr class = "active" style="cursor:pointer;"onclick='document.location.href="bill?nb=${bill.id}";'>
         <td>${bill.numero}</td>
         <td>${bill.createdOn }</td>
         <td>${bill.price }€</td>
      </tr>

      </c:forEach>
   </tbody>
   
</table>
<jsp:include page="../footer.jsp"></jsp:include>