<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- Font Awesome Icons -->
    <link async href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!-- Theme style -->
		<link  rel="stylesheet" href="/salescode/assets/css/bootstrap.min.css" type="text/css" media="print, projection, screen" />
		<link  rel="stylesheet" href="/salescode/assets/css/style.css" type="text/css" media="print, projection, screen" />
		<link  rel="stylesheet" href="/salescode/assets/css/green.css" type="text/css" media="print, projection, screen" />
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <title>Paiements</title>
  </head>

  <body class="skin-green sidebar-mini"  onload="document.forms['paypalForm'].submit();">
    <div class="wrapper">

      <!-- Main Header -->
      <header class="main-header">

        <!-- Logo -->
        <a href="/salescode/dashboard" class="logo">
          <!-- mini logo for sidebar mini 50x50 pixels -->
          <span class="logo-mini"><b>SC</b></span>
          <!-- logo for regular state and mobile devices -->
          <span class="logo-lg">SalesCode</span>
        </a>

        <!-- Header Navbar -->
		<jsp:include page="../navbar-top.jsp" />
		<jsp:include page="../navbar-left.jsp" />
        

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Main content -->
        <section class="content">
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<c:forEach items="${listPurchase}" var="purchase">
						<c:set var="sumPrice" value="${sumPrice + purchase.value.repository.price}" />
				</c:forEach>

<form name="paypalForm" action="https://api-3t.sandbox.paypal.com/nvp" method="post">
 <input type="hidden" name="cmd" value="_xclick" />
 <input type="hidden" name="business" value="teamsalescode_api1.gmail.com" />
 <input type="hidden" name="password" value="A7V747UVPA7P9UT2" />
 <input type="hidden" name="custom" value="1123" />
 <input type="hidden" name="item_name" value="Repository" />
 <input type="hidden" name="amount" value="${sumPrice}"/>
 <input type="hidden" name="rm" value="1" />
 <input type="hidden" name="return" value="http://localhost:8080/PaypalGS/paypalResponse.jsp" />
 <input type="hidden" name="cancel_return" value="http://localhost:8080/PaypalGS/paypalResponseCancel.jsp" />
 <input type="hidden" name="cert_id" value="AkqvibwMPIB0J2WQ.u7VSWi9kgfAAK03b6syL5I71J0yr0vVxMgP4pNh" />
</form>
<jsp:include page="../footer.jsp"></jsp:include>