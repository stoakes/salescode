<%@ page pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="../header.jsp">
  <jsp:param name="title" >
      <jsp:attribute name="value" >
        Mon Panier
      </jsp:attribute>
  </jsp:param>
</jsp:include>
<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
<c:if test="${cancel==1}">
  <div class="alert alert-danger bs-alert-old-docs">Le paiement a été annulé, vous pouvez continuer vos achats normalement.</div>
</c:if>

<c:if test="${empt==1}">
	<div class="alert alert-error bs-alert-old-docs">
		Votre panier est vide. Cliquez  <a href="marketplace">ici</a> pour revenir en arrière.
	</div>
</c:if>
<c:if test="${yet==1}">
	<div class="alert alert-warning bs-alert-old-docs">
		Vous possédez déjà cet article. Cliquez  <a href="marketplace">ici</a> pour revenir en arrière.
	</div>
</c:if>
<c:if test="${purchased==1}">
	<div class="alert alert-warning bs-alert-old-docs">
		Vous possédez déjà cet article. Cliquez  <a href="marketplace">ici</a> pour revenir en arrière.
	</div>
</c:if>
<div class="container">
	<table id="cart" class="table table-hover table-condensed">
    				<thead>
						<tr>
							<th style="width:50%">Produit</th>
							<th style="width:10%">Prix</th>
							<%--<th style="width:8%">Quantité</th>--%>
							<th style="width:22%" class="text-center">Sous-total</th>
							<th style="width:10%"></th>
						</tr>
					</thead>
					<tbody>
						<c:forEach items="${listPurchase}" var="purchase">
						
						<tr>
							<td data-th="Product">
								<div class="row">
									<div class="col-sm-2 hidden-xs"><img src="${purchase.value.repository.image }" alt="${purchase.value.repository.title}" class="img-responsive"/></div>
									<div class="col-sm-10">
										<h4 class="nomargin">${purchase.value.repository.title }</h4>
										<p>${purchase.value.repository.description }</p>
									</div>
								</div>
							</td>
							<td data-th="Price">${purchase.value.repository.price}</td>
							<%-- <td data-th="Quantity">
								<input type="number" class="form-control text-center" value="${purchase.value.quantity}">
							</td>--%>
							<td data-th="Subtotal" class="text-center">${purchase.value.repository.price}</td>
							<td class="actions" data-th="">
								<a href="/salescode/ShoppingCart?q=${purchase.value.quantity}&ir=${purchase.value.repository.id }" class="btn btn-info btn-sm"><i class="fa fa-refresh"></i></a>
								<a  class="btn btn-danger btn-sm" href="/salescode/ShoppingCart?rm=${purchase.value.repository.id}"><i class="fa fa-trash-o"></i></a>								
							</td>
						</tr>
						<c:set var="sumPrice" value="${sumPrice + purchase.value.repository.price}" />
						
						</c:forEach>
					</tbody>
					<tfoot>
						<tr class="visible-xs">
							<td class="text-center"><strong>Total ${sumPrice + purchase.value.repository.price}€</strong></td>
						</tr>
						<tr>
							<td><a href="/salescode/marketplace" class="btn btn-warning"><i class="fa fa-angle-left"></i> Continuer les achats</a></td>
							<td colspan="2" class="hidden-xs"></td>
							<td class="hidden-xs text-center"><strong>Total ${sumPrice + purchase.value.repository.price}€</strong></td>
							<td><a href="/salescode/ShoppingCart?valid=1" class="btn btn-success btn-block">Valider <i class="fa fa-angle-right"></i></a></td>
						</tr>
					</tfoot>
				</table>
</div>

<jsp:include page="../footer.jsp"></jsp:include>