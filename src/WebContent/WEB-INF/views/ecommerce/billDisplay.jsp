<%@ page pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="../header.jsp">
  <jsp:param name="title" >
      <jsp:attribute name="value" >
        Mes Factures
      </jsp:attribute>
  </jsp:param>
</jsp:include>
<div class="container">
    <div class="row">
        <div class="col-xs-12">
    		<div class="invoice-title">
    			<h2>Facture</h2><h3 class="pull-right">Numero # ${bill.numero}</h3>
    		</div>
    		<hr>
    		<div class="row">
    			<div class="col-xs-6">
    				<address>
    				<strong>Facturé à:</strong><br>
    					${user.userDetails.name} ${user.userDetails.surname}<br>
    					${user.userDetails.city},<br>
    					${user.userDetails.country}<br>				
    				</address>
    			</div>
    			<div class="col-xs-6 text-right">
    				<img alt="Salescode" src="http://img11.hostingpics.net/pics/208803logo.png">
    				<address>
        			<strong>Facturé par:</strong><br>
    					Team Salescode<br>
    					2 rue Charles Camichel<br>
    					Toulouse, 31000
    				</address>
    			</div>
    		</div>
    		<div class="row">
    			<div class="col-xs-6">
    				<address>
    					<strong>Paiement</strong><br>
    					Paypal<br>
    					${user.email }
    				</address>
    			</div>
    			<div class="col-xs-6 text-right">
    				<address>
    					<strong>Date:</strong><br>
    					${bill.createdOn }<br><br>
    				</address>
    			</div>
    		</div>
    	</div>
    </div>
    
    <div class="row">
    	<div class="col-md-12">
    		<div class="panel panel-default">
    			<div class="panel-heading">
    				<h3 class="panel-title"><strong>Résumé de la commande</strong></h3>
    			</div>
    			<div class="panel-body">
    				<div class="table-responsive">
    					<table class="table table-condensed">
    						<thead>
                                <tr>
        							<td><strong>Item</strong></td>
        							<td class="text-center"><strong>Prix</strong></td>
        							<td class="text-center"><strong>Description</strong></td>
        							<td class="text-right"><strong>Total</strong></td>
                                </tr>
    						</thead>
    						<tbody>
    							<!-- foreach ($order->lineItems as $line) or some such thing here -->
    							<tr>
    								<td>${repo.title}</td>
    								<td class="text-center">${repo.price }</td>
    								<td class="text-center">${repo.description }</td>
    								<td class="text-right">${repo.price}</td>
    							</tr>
                                
    						</tbody>
    					</table>
    					
    				</div>
    				
    			</div>
    		</div>
    		<p>« TVA non applicable, art. 293 B du CGI »</p>
    	</div>
    </div>
</div>
<jsp:include page="../footer.jsp"></jsp:include>