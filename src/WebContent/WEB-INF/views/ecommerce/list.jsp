<%@ page pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<jsp:include page="../header.jsp">
	<jsp:param name="title">
		<jsp:attribute name="value">
        Market Place
      </jsp:attribute>
	</jsp:param>
</jsp:include>
<c:if test="${success==1}">
	<div class="alert alert-success bs-alert-old-docs">
		Votre paiement a bien été validé. Vous pouvez retrouver vos dêpots
		achetés <a href="dashboard">ici</a>.
	</div>
</c:if>
<div class="container">
	<div id="products" class="row list-group">
		<c:forEach items="${listRepo}" var="repo">
			<div class="item  col-xs-4 col-lg-4"  style="cursor:pointer;"onclick='document.location.href="marketplace?p=${repo.id}";'>
				<div class="thumbnail">
					<img class="group list-group-image" src="${repo.image }"
						alt="${repo.title }" width="150px" height="150px"/>
					<div class="caption">
						<h4 class="group inner list-group-item-heading">
							${repo.title}</h4>
						<p class="group inner list-group-item-text">
							${repo.description}</p>
						<div class="row">
							<div class="col-xs-12 col-md-6">
								<p class="lead">${repo.price}€</p>
							</div>
         						<div class="col-xs-12 col-md-6">
								<a class="btn btn-success"
									href="/salescode/ShoppingCart?product=${repo.id}">Ajouter au Panier</a>
								</div>
								
							
						</div>
					</div>
				</div>
			</div>
		</c:forEach>
	</div>
	<%--Affichage de la pagination du catalogue--%>
	<div class="col-md-12 text-center">
		<ul class="pagination pagination-lg">
			<c:forEach begin="1" end="${pageNumber}" var="i">
				<c:choose>
					<c:when test="${currentPage eq i}">
						<li class="active"><a href="#">${i}</a></li>
					</c:when>

					<c:otherwise>
						<li><a href="/salescode/marketplace?page=${i}">${i}</a></li>
					</c:otherwise>
				</c:choose>
			</c:forEach>
		</ul>
	</div>
</div>

<jsp:include page="../footer.jsp"></jsp:include>