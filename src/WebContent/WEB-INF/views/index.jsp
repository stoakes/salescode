<%@ page pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="header.jsp">
  <jsp:param name="title" >
      <jsp:attribute name="value" >
        Market Place
      </jsp:attribute>
  </jsp:param>
</jsp:include>

	${!empty message ? '<div class="alert alert-danger">'.concat(message).concat('</div>') : ''}

<h1>Salescode</h1>

<% if(session.getAttribute("user") == null){ %>
<h4><a href="/salescode/connexion">Connexion</a></h4>
<h4><a href="/salescode/inscription">Inscription</a></h4>
<h4><a href="/salescode/repositories?op=new">Ajout Repository</a></h4>
<h4><a href="/salescode/marketplace">Lister Repository</a></h4>
<% }else{ %>
	<h4><a href="/salescode/repositories?op=new">Ajout Repository</a></h4>
	<h4><a href="/salescode/marketplace">Lister Repository</a></h4>
	<h4><a href="/salescode/connexion?op=sd">Deconnexion</a></h4>
	
<% } %>
<jsp:include page="footer.jsp"></jsp:include>
